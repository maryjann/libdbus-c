/*
 * ----------------------------------------------------------------------------
 * "THE BEER-WARE LICENSE" (Revision 42):
 * <nowacki.jakub@gmail.com> wrote this file. As long as you retain this notice you
 * can do whatever you want with this stuff. If we meet some day, and you think
 * this stuff is worth it, you can buy me a beer in return Jakub Nowacki
 * ----------------------------------------------------------------------------
 */

#include <dbus/dbus.h>

#include <ctools/log.h>

#include "dbus-common.h"
#include "dbus-common-debug.h"
#include "dbus-common-helpers.h"

#include <CVector.h>

static int dbus_common_append_basic( DBusMessageIter *iter, int type, void *data );
static int dbus_common_append_struct( DBusMessageIter *iter, const char *format, void *data );
static int dbus_common_append_array( DBusMessageIter *iter, const char *format, CVectorHandle data );
static int dbus_common_append_variant( DBusMessageIter *iter, void *data );
static int dbus_common_append_dict_entry( DBusMessageIter *iter, const char *format, void *data );

int dbus_common_build_message_args( DBusMessageIter *iter, DBusMessageIter *iter_up, int parent, const char *format, va_list va )
{
    if( iter == NULL )
    {
        log_error( "%s(): invalid arguments", __func__ );
        return -1;
    }

    int ret = 0;
    size_t idx = 0;
    char sign[strlen(format)];
    void *data = NULL;

    log_info( "%sstarting message builder, signature: %s", dbus_common_get_indent( MSG_LEVEL_POST_UP ), format );

    while( format[idx] != '\0' )
    {
        idx += dbus_common_decode_complete_type( &format[idx], sign );

        log_info( "%snow building: %s", dbus_common_get_indent( MSG_LEVEL_POST_UP ), dbus_common_debug_msg(sign) );
        data = va_arg( va, void * );

        switch( sign[0] )
        {
            case DBUS_TYPE_BYTE:
            case DBUS_TYPE_BOOLEAN:
            case DBUS_TYPE_INT16:
            case DBUS_TYPE_UINT16:
            case DBUS_TYPE_INT32:
            case DBUS_TYPE_UINT32:
            case DBUS_TYPE_INT64:
            case DBUS_TYPE_UINT64:
            case DBUS_TYPE_DOUBLE:
            case DBUS_TYPE_UNIX_FD:
                /*TODO - add error handlig*/
                dbus_common_append_basic( iter, sign[0], data );
                log_info( "%svalue: %s", dbus_common_get_indent( MSG_LEVEL_STAY ), dbus_common_debug_basic( sign, data ) );
                break;
            case DBUS_TYPE_STRING:
                /*TODO - add error handlig*/
                dbus_common_append_basic( iter, sign[0], &data );
                log_info( "%svalue: %s", dbus_common_get_indent( MSG_LEVEL_STAY ), dbus_common_debug_basic( sign, &data ) );
                break;
            case DBUS_TYPE_ARRAY:
                /*TODO - add error handlig*/
                dbus_common_append_array( iter, sign, data ); 
                break;
            case DBUS_TYPE_VARIANT:
                /*TODO - add error handlig*/
                dbus_common_append_variant( iter, data ); 
                break;
            case DBUS_STRUCT_BEGIN_CHAR:
                /*TODO - add error handlig*/
                dbus_common_append_struct( iter, sign, data );
                break;
            case DBUS_DICT_ENTRY_BEGIN_CHAR:
                /*TODO - should never happen - dict entry should be handled while appending array*/
                break;
        }

        log_info( "%s%s - append", dbus_common_get_indent( MSG_LEVEL_PRE_DOWN ), dbus_common_debug_msg(sign) );

    }

    log_info( "%smessage(%s) built", dbus_common_get_indent( MSG_LEVEL_PRE_DOWN ), format );

    return 0;
}

static int dbus_common_append_basic( DBusMessageIter *iter, int type, void *data )
{
    return dbus_message_iter_append_basic( iter, type, data );
}

static int dbus_common_append_struct( DBusMessageIter *iter, const char *format, void *data )
{
    int ret = 0;
    size_t idx = 1;

    DBusMessageIter sub;
    char type[strlen(format)];
                
    ret = dbus_message_iter_open_container( iter, DBUS_TYPE_STRUCT, NULL, &sub ); 

    while( idx < strlen(format) )
    {
        idx += dbus_common_decode_complete_type( &format[idx], type );
        switch( (int)type[0] )
        {
            case DBUS_TYPE_BYTE:
                log_info( "%sappending struct element, type: %s", dbus_common_get_indent( MSG_LEVEL_POST_UP ), dbus_common_debug_msg(type) );
                /*TODO - add error handlig*/
                dbus_common_append_basic( &sub, type[0], data ); 
                log_info( "%svalue: %s", dbus_common_get_indent( MSG_LEVEL_POST_DOWN ), dbus_common_debug_basic( type, data ) );
                data += sizeof(dbus_type_byte); 
                break;
            case DBUS_TYPE_BOOLEAN:
                log_info( "%sappending struct element, type: %s", dbus_common_get_indent( MSG_LEVEL_POST_UP ), dbus_common_debug_msg(type) );
                /*TODO - add error handlig*/
                dbus_common_append_basic( &sub, type[0], data ); 
                log_info( "%svalue: %s", dbus_common_get_indent( MSG_LEVEL_POST_DOWN ), dbus_common_debug_basic( type, data ) );
                data += sizeof(dbus_type_boolean);
                break;
            case DBUS_TYPE_INT16:
                log_info( "%sappending struct element, type: %s", dbus_common_get_indent( MSG_LEVEL_POST_UP ), dbus_common_debug_msg(type) );
                /*TODO - add error handlig*/
                dbus_common_append_basic( &sub, type[0], data ); 
                log_info( "%svalue: %s", dbus_common_get_indent( MSG_LEVEL_POST_DOWN ), dbus_common_debug_basic( type, data ) );
                data += sizeof(dbus_type_int16);
                break;
            case DBUS_TYPE_UINT16:
                log_info( "%sappending struct element, type: %s", dbus_common_get_indent( MSG_LEVEL_POST_UP ), dbus_common_debug_msg(type) );
                /*TODO - add error handlig*/
                dbus_common_append_basic( &sub, type[0], data ); 
                log_info( "%svalue: %s", dbus_common_get_indent( MSG_LEVEL_POST_DOWN ), dbus_common_debug_basic( type, data ) );
                data += sizeof(dbus_type_uint16);
                break;
            case DBUS_TYPE_INT32:
                log_info( "%sappending struct element, type: %s", dbus_common_get_indent( MSG_LEVEL_POST_UP ), dbus_common_debug_msg(type) );
                /*TODO - add error handlig*/
                dbus_common_append_basic( &sub, type[0], data ); 
                log_info( "%svalue: %s", dbus_common_get_indent( MSG_LEVEL_POST_DOWN ), dbus_common_debug_basic( type, data ) );
                data += sizeof(dbus_type_int32);
                break;
            case DBUS_TYPE_UINT32:
                log_info( "%sappending struct element, type: %s", dbus_common_get_indent( MSG_LEVEL_POST_UP ), dbus_common_debug_msg(type) );
                /*TODO - add error handlig*/
                dbus_common_append_basic( &sub, type[0], data ); 
                log_info( "%svalue: %s", dbus_common_get_indent( MSG_LEVEL_POST_DOWN ), dbus_common_debug_basic( type, data ) );
                data += sizeof(dbus_type_uint32);
                break;
            case DBUS_TYPE_INT64:
                log_info( "%sappending struct element, type: %s", dbus_common_get_indent( MSG_LEVEL_POST_UP ), dbus_common_debug_msg(type) );
                /*TODO - add error handlig*/
                dbus_common_append_basic( &sub, type[0], data ); 
                log_info( "%svalue: %s", dbus_common_get_indent( MSG_LEVEL_POST_DOWN ), dbus_common_debug_basic( type, data ) );
                data += sizeof(dbus_type_int64);
                break;
            case DBUS_TYPE_UINT64:
                log_info( "%sappending struct element, type: %s", dbus_common_get_indent( MSG_LEVEL_POST_UP ), dbus_common_debug_msg(type) );
                /*TODO - add error handlig*/
                dbus_common_append_basic( &sub, type[0], data ); 
                log_info( "%svalue: %s", dbus_common_get_indent( MSG_LEVEL_POST_DOWN ), dbus_common_debug_basic( type, data ) );
                data += sizeof(dbus_type_uint64);
                break;
            case DBUS_TYPE_DOUBLE:
                log_info( "%sappending struct element, type: %s", dbus_common_get_indent( MSG_LEVEL_POST_UP ), dbus_common_debug_msg(type) );
                /*TODO - add error handlig*/
                dbus_common_append_basic( &sub, type[0], data ); 
                log_info( "%svalue: %s", dbus_common_get_indent( MSG_LEVEL_POST_DOWN ), dbus_common_debug_basic( type, data ) );
                data += sizeof(dbus_type_double);
                break;
            case DBUS_TYPE_STRING:
                log_info( "%sappending struct element, type: %s", dbus_common_get_indent( MSG_LEVEL_POST_UP ), dbus_common_debug_msg(type) );
                /*TODO - add error handlig*/
                dbus_common_append_basic( &sub, type[0], data );
                log_info( "%svalue: %s", dbus_common_get_indent( MSG_LEVEL_POST_DOWN ), dbus_common_debug_basic( type, data ) );
                data += sizeof(dbus_type_string);
                break;
            case DBUS_TYPE_UNIX_FD:
                log_info( "%sappending struct element, type: %s", dbus_common_get_indent( MSG_LEVEL_POST_UP ), dbus_common_debug_msg(type) );
                /*TODO - add error handlig*/
                dbus_common_append_basic( &sub, type[0], data );
                log_info( "%svalue: %s", dbus_common_get_indent( MSG_LEVEL_POST_DOWN ), dbus_common_debug_basic( type, data ) );
                data += sizeof(dbus_type_unix_fd);
                break;
            case DBUS_TYPE_ARRAY:
                log_info( "%sappending struct element, type: %s", dbus_common_get_indent( MSG_LEVEL_POST_UP ), dbus_common_debug_msg(type) );
                /*TODO - add error handlig*/
                dbus_common_append_array( &sub, type, data );
                dbus_common_get_indent( MSG_LEVEL_POST_DOWN );
                data += sizeof( dbus_type_array );
                break;
            case DBUS_TYPE_VARIANT:
                log_info( "%sappending struct element, type: %s", dbus_common_get_indent( MSG_LEVEL_POST_UP ), dbus_common_debug_msg(type) );
                /*TODO - add error handlig*/
                dbus_common_append_variant( &sub, data );
                dbus_common_get_indent( MSG_LEVEL_POST_DOWN );
                data += sizeof( dbus_type_variant ); 
                break;
            case DBUS_STRUCT_BEGIN_CHAR:
                log_info( "%sappending struct element, type: %s", dbus_common_get_indent( MSG_LEVEL_POST_UP ), dbus_common_debug_msg(type) );
                /*TODO - add error handlig*/
                dbus_common_append_struct( &sub, type, data );
                dbus_common_get_indent( MSG_LEVEL_POST_DOWN );
                data += dbus_common_calculate_complete_type_size( type );
                break;
            case DBUS_DICT_ENTRY_BEGIN_CHAR:
                log_error( "Appending %s as struct element.", dbus_common_debug_msg(type) );
                /*TODO - should never happen - dict entry should be handled while appending array*/
                break;
            default:
                break;
        }
    }
    
    ret = dbus_message_iter_close_container( iter, &sub );
    
    return 0;
}

static int dbus_common_append_array( DBusMessageIter *iter, const char *format, CVectorHandle data )
{
    int ret = 0;
    size_t size = 0;
    
    DBusMessageIter sub;
    ret = dbus_message_iter_open_container( iter, DBUS_TYPE_ARRAY, &format[1], &sub ); 

    ret = CVectorGetSize( data, &size );
    if( ret != 0 )
    {
        log_error( "%s(): CVectorGetSize failed with: %d", __func__, ret );
        return -1;
    }

    log_info( "%sbuilding array of %s", dbus_common_get_indent( MSG_LEVEL_POST_UP ), dbus_common_debug_msg(&format[1]) );

    for( size_t idx = 0; idx < size; ++idx )
    {
        void *el = NULL;
        ret = CVectorGetElementptr( data, &el, idx );
        if( ret != 0 )
        {
            log_error( "%s(): CVectorGetElement failed with: %d", __func__, ret );
            return -2;
        }
        switch( format[1] )
        {
            case DBUS_STRUCT_BEGIN_CHAR:
                /*TODO - add error handling*/
                dbus_common_append_struct( &sub, &format[1], el );
                break;
            case DBUS_DICT_ENTRY_BEGIN_CHAR:
                /*TODO - add error handling*/
                dbus_common_append_dict_entry( &sub, &format[1], el );
                break;
            case DBUS_TYPE_ARRAY:
                /*TODO - add error handling*/
                dbus_common_append_array( &sub, &format[1], el );
                break;
            case DBUS_TYPE_VARIANT:
                dbus_common_append_variant( &sub, el );
                break;
            default:
                /*TODO - add error handlig*/
                log_info( "%svalue: %s", dbus_common_get_indent( MSG_LEVEL_STAY ), dbus_common_debug_basic( &format[1], el ) );
                dbus_common_append_basic( &sub, format[1], el );
                break;
        }
    }

    dbus_common_get_indent( MSG_LEVEL_PRE_DOWN );

    int vector_flags;
    CVectorGetFlags( data, &vector_flags );
    vector_flags = vector_flags & !CVECTOR_NO_RELOCATION;
    CVectorSetFlags( data, vector_flags );

    ret = dbus_message_iter_close_container( iter, &sub );
    
    return 0;
}

static int dbus_common_append_variant( DBusMessageIter *iter, void *data )
{
    int ret = 0;
    
    dbus_type_variant *variant = (dbus_type_variant *)data;
    
    DBusMessageIter sub;
    ret = dbus_message_iter_open_container( iter, DBUS_TYPE_VARIANT, variant->signature, &sub );

    log_info( "%sappending variant element, type: %s", dbus_common_get_indent( MSG_LEVEL_POST_UP ), dbus_common_debug_msg(variant->signature) );
    
    switch( variant->signature[0] )
    {
        case DBUS_STRUCT_BEGIN_CHAR:
            dbus_common_append_struct( &sub, variant->signature, variant->STRUCT );
            break;
        case DBUS_DICT_ENTRY_BEGIN_CHAR:
            /*TODO - should never happen - dict entry should be handled while appending array*/
            break;
        case DBUS_TYPE_STRING:
            dbus_common_append_basic( &sub, variant->signature[0], &variant->DATA );
            log_info( "%svalue: %s", dbus_common_get_indent( MSG_LEVEL_STAY ), dbus_common_debug_basic( variant->signature, &variant->DATA ) );
            break;
        case DBUS_TYPE_ARRAY:
            dbus_common_append_array( &sub, variant->signature, variant->ARRAY );
            break;
        default:
            /*TODO - add error handlig*/
            dbus_common_append_basic( &sub, variant->signature[0], variant->DATA );
            log_info( "%svalue: %s", dbus_common_get_indent( MSG_LEVEL_STAY ), dbus_common_debug_basic( variant->signature, variant->DATA ) );
            break;
    }
    
    dbus_common_get_indent( MSG_LEVEL_PRE_DOWN );
    
    ret = dbus_message_iter_close_container( iter, &sub );

    return 0;
}

static int dbus_common_append_dict_entry( DBusMessageIter *iter, const char *format, void *data )
{
    int ret = 0;
    size_t idx = 1;

    DBusMessageIter sub;
    char type[strlen(format)];
                
    ret = dbus_message_iter_open_container( iter, DBUS_TYPE_DICT_ENTRY, NULL, &sub ); 

    while( idx < strlen(format) )
    {
        idx += dbus_common_decode_complete_type( &format[idx], type );
        switch( (int)type[0] )
        {
            case DBUS_TYPE_BYTE:
                log_info( "%sappending dict entry element, type: %s", dbus_common_get_indent( MSG_LEVEL_POST_UP ), dbus_common_debug_msg(type) );
                /*TODO - add error handlig*/
                dbus_common_append_basic( &sub, type[0], data ); 
                log_info( "%svalue: %s", dbus_common_get_indent( MSG_LEVEL_POST_DOWN ), dbus_common_debug_basic( type, data ) );
                data += sizeof(dbus_type_byte); 
                break;
            case DBUS_TYPE_BOOLEAN:
                log_info( "%sappending dict entry element, type: %s", dbus_common_get_indent( MSG_LEVEL_POST_UP ), dbus_common_debug_msg(type) );
                /*TODO - add error handlig*/
                dbus_common_append_basic( &sub, type[0], data ); 
                log_info( "%svalue: %s", dbus_common_get_indent( MSG_LEVEL_POST_DOWN ), dbus_common_debug_basic( type, data ) );
                data += sizeof(dbus_type_boolean);
                break;
            case DBUS_TYPE_INT16:
                log_info( "%sappending dict entry element, type: %s", dbus_common_get_indent( MSG_LEVEL_POST_UP ), dbus_common_debug_msg(type) );
                /*TODO - add error handlig*/
                dbus_common_append_basic( &sub, type[0], data ); 
                log_info( "%svalue: %s", dbus_common_get_indent( MSG_LEVEL_POST_DOWN ), dbus_common_debug_basic( type, data ) );
                data += sizeof(dbus_type_int16);
                break;
            case DBUS_TYPE_UINT16:
                log_info( "%sappending dict entry element, type: %s", dbus_common_get_indent( MSG_LEVEL_POST_UP ), dbus_common_debug_msg(type) );
                /*TODO - add error handlig*/
                dbus_common_append_basic( &sub, type[0], data ); 
                log_info( "%svalue: %s", dbus_common_get_indent( MSG_LEVEL_POST_DOWN ), dbus_common_debug_basic( type, data ) );
                data += sizeof(dbus_type_uint16);
                break;
            case DBUS_TYPE_INT32:
                log_info( "%sappending dict entry element, type: %s", dbus_common_get_indent( MSG_LEVEL_POST_UP ), dbus_common_debug_msg(type) );
                /*TODO - add error handlig*/
                dbus_common_append_basic( &sub, type[0], data ); 
                log_info( "%svalue: %s", dbus_common_get_indent( MSG_LEVEL_POST_DOWN ), dbus_common_debug_basic( type, data ) );
                data += sizeof(dbus_type_int32);
                break;
            case DBUS_TYPE_UINT32:
                log_info( "%sappending dict entry element, type: %s", dbus_common_get_indent( MSG_LEVEL_POST_UP ), dbus_common_debug_msg(type) );
                /*TODO - add error handlig*/
                dbus_common_append_basic( &sub, type[0], data ); 
                log_info( "%svalue: %s", dbus_common_get_indent( MSG_LEVEL_POST_DOWN ), dbus_common_debug_basic( type, data ) );
                data += sizeof(dbus_type_uint32);
                break;
            case DBUS_TYPE_INT64:
                log_info( "%sappending dict entry element, type: %s", dbus_common_get_indent( MSG_LEVEL_POST_UP ), dbus_common_debug_msg(type) );
                /*TODO - add error handlig*/
                dbus_common_append_basic( &sub, type[0], data ); 
                log_info( "%svalue: %s", dbus_common_get_indent( MSG_LEVEL_POST_DOWN ), dbus_common_debug_basic( type, data ) );
                data += sizeof(dbus_type_int64);
                break;
            case DBUS_TYPE_UINT64:
                log_info( "%sappending dict entry element, type: %s", dbus_common_get_indent( MSG_LEVEL_POST_UP ), dbus_common_debug_msg(type) );
                /*TODO - add error handlig*/
                dbus_common_append_basic( &sub, type[0], data ); 
                log_info( "%svalue: %s", dbus_common_get_indent( MSG_LEVEL_POST_DOWN ), dbus_common_debug_basic( type, data ) );
                data += sizeof(dbus_type_uint64);
                break;
            case DBUS_TYPE_DOUBLE:
                log_info( "%sappending dict entry element, type: %s", dbus_common_get_indent( MSG_LEVEL_POST_UP ), dbus_common_debug_msg(type) );
                /*TODO - add error handlig*/
                dbus_common_append_basic( &sub, type[0], data ); 
                log_info( "%svalue: %s", dbus_common_get_indent( MSG_LEVEL_POST_DOWN ), dbus_common_debug_basic( type, data ) );
                data += sizeof(dbus_type_double);
                break;
            case DBUS_TYPE_STRING:
                log_info( "%sappending dict entry element, type: %s", dbus_common_get_indent( MSG_LEVEL_POST_UP ), dbus_common_debug_msg(type) );
                /*TODO - add error handlig*/
                dbus_common_append_basic( &sub, type[0], data );
                log_info( "%svalue: %s", dbus_common_get_indent( MSG_LEVEL_POST_DOWN ), dbus_common_debug_basic( type, data ) );
                data += sizeof(dbus_type_string);
                break;
            case DBUS_TYPE_UNIX_FD:
                log_info( "%sappending dict entry element, type: %s", dbus_common_get_indent( MSG_LEVEL_POST_UP ), dbus_common_debug_msg(type) );
                /*TODO - add error handlig*/
                dbus_common_append_basic( &sub, type[0], data );
                log_info( "%svalue: %s", dbus_common_get_indent( MSG_LEVEL_POST_DOWN ), dbus_common_debug_basic( type, data ) );
                data += sizeof(dbus_type_unix_fd);
                break;
            case DBUS_TYPE_ARRAY:
                log_info( "%sappending dict entry element, type: %s", dbus_common_get_indent( MSG_LEVEL_POST_UP ), dbus_common_debug_msg(type) );
                /*TODO - add error handlig*/
                dbus_common_append_array( &sub, type, data );
                dbus_common_get_indent( MSG_LEVEL_POST_DOWN );
                data += sizeof( dbus_type_array );
                break;
            case DBUS_TYPE_VARIANT:
                log_info( "%sappending dict entry element, type: %s", dbus_common_get_indent( MSG_LEVEL_POST_UP ), dbus_common_debug_msg(type) );
                /*TODO - add error handlig*/
                dbus_common_append_variant( &sub, data );
                dbus_common_get_indent( MSG_LEVEL_POST_DOWN );
                data += sizeof( dbus_type_variant ); 
                break;
            case DBUS_STRUCT_BEGIN_CHAR:
                log_info( "%sappending dict entry element, type: %s", dbus_common_get_indent( MSG_LEVEL_POST_UP ), dbus_common_debug_msg(type) );
                /*TODO - add error handlig*/
                dbus_common_append_struct( &sub, type, data );
                dbus_common_get_indent( MSG_LEVEL_POST_DOWN );
                data += dbus_common_calculate_complete_type_size( type );
                break;
            case DBUS_DICT_ENTRY_BEGIN_CHAR:
                log_error( "Appending %s as dict entry element.", dbus_common_debug_msg(type) );
                /*TODO - should never happen - dict entry should be handled while appending array*/
                break;
            default:
                break;
        }
    }
    
    ret = dbus_message_iter_close_container( iter, &sub );
    
    return 0;
}
