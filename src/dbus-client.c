/*
 * ----------------------------------------------------------------------------
 * "THE BEER-WARE LICENSE" (Revision 42):
 * <nowacki.jakub@gmail.com> wrote this file. As long as you retain this notice you
 * can do whatever you want with this stuff. If we meet some day, and you think
 * this stuff is worth it, you can buy me a beer in return Jakub Nowacki
 * ----------------------------------------------------------------------------
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <stdarg.h>
#include <unistd.h>

#include <dbus/dbus.h>

#include <ctools/io.h>
#include <ctools/log.h>
                    
#include "dbus-common.h"
#include "dbus-client.h"

typedef enum dbus_block_type_e
{
    dbus_block = 0,
    dbus_no_block
} dbus_block_type_t;

typedef struct dbus_client_interface_s
{
    map_t signals;
    map_t replies;
} dbus_client_interface_t;

typedef struct dbus_client_signal_s
{
    DBusMessage *message;
    dbus_client_signal_handler_f signal_handler;
    void *data;
} dbus_client_signal_t;

typedef struct dbus_client_reply_s
{
    DBusMessage *message;
    dbus_client_reply_handler_f reply_handler;
} dbus_client_reply_t;

static DBusHandlerResult dbus_client_filter( DBusConnection *connection, DBusMessage *message, void *data );

static dbus_bool_t dbus_client_add_watch( DBusWatch * watch, void * data );
static void dbus_client_remove_watch( DBusWatch * watch, void * data );
static void dbus_client_toggle_watch( DBusWatch * watch, void * data );

static void dbus_client_handle_dispatch_status( DBusConnection *connection, DBusDispatchStatus status, void *data );

static void dbus_client_unregister_handler( DBusConnection  *connection, void *data );

static DBusHandlerResult dbus_client_message_handler( DBusConnection *connection, DBusMessage *message, void *data );

static void dbus_client_reply_handler( DBusPendingCall *pending, void *data );

static int dbus_client_read_cb( int fd, void *data );
static int dbus_client_timeout_cb( int fd, void *data );

static int name_compare( const void *name_1, const void *name_2 );

static int _dbus_client_get_signal_args_on_interface( dbus_client_t *dbus_client, char * const ifce_name, char * const name, const char * format, va_list va );
static int _dbus_client_get_reply_args_on_interface( dbus_client_t *dbus_client, uint32_t *id, char * const ifce_name, char * const name, const char * format, va_list va );

static int _dbus_client_call_method_on_interface( dbus_client_t *dbus_client, dbus_block_type_t block, uint32_t *id, char * const ifce_name, char * const name, const char *format, va_list va );
static int _dbus_client_call_method_with_reply_on_interface( dbus_client_t *dbus_client, dbus_block_type_t block, uint32_t *id, char * const ifce_name, char * const name, const char *format, va_list va );

//Static variables -- Start
static DBusConnection *connection;
static DBusConnection *connection_blk;
static DBusError error;

static DBusObjectPathVTable dbus_client_vtable = {
    .unregister_function = dbus_client_unregister_handler,
    .message_function = dbus_client_message_handler,
    NULL,
};

static io_config_t dbus_client_io_config = {
    .cb = {
        .read_cb = dbus_client_read_cb,
        .write_cb = NULL,
        .timeout_cb = dbus_client_timeout_cb,
    },
    .timeout = {
        .tv_sec = 1,
        .tv_nsec = 0,
    }
};

//Static variables -- End

int dbus_client_init( dbus_bus_type_t bus_type )
{
    int ret = 0;

    log_debug( "%s(): enter", __func__ );
    
    dbus_error_init( &error );

    connection = dbus_bus_get( dbus_common_map_bus_type( bus_type ), &error );
    if( connection == NULL )
    {
        log_error( "%s(): dbus_bus_get(connection) failed with: %s", __func__, error.message );
        ret = -1;
        goto dbus_client_error_cleanup;
    }

    connection_blk = dbus_bus_get( dbus_common_map_bus_type( bus_type ), &error );
    if( connection_blk == NULL )
    {
        log_error( "%s(): dbus_bus_get(connection_blk) failed with: %s", __func__, error.message );
        ret = -1;
        goto dbus_client_error_cleanup;
    }

    dbus_bus_register( connection, &error );
    dbus_bus_register( connection_blk, &error );

    dbus_connection_set_exit_on_disconnect( connection, FALSE );
    dbus_connection_set_exit_on_disconnect( connection_blk, FALSE );

    ret = dbus_connection_add_filter( connection, dbus_client_filter, NULL, NULL );
    if( ret == FALSE )
    {
        log_error( "%s(): dbus_connection_add_filter failed", __func__ );
        ret = -2;
        goto dbus_client_get_cleanup;
    }
   
    ret = dbus_connection_set_watch_functions( connection, dbus_client_add_watch, dbus_client_remove_watch, dbus_client_toggle_watch, NULL, NULL );
    if( ret == FALSE )
    {
        log_error( "%s(): dbus_connection_set_watch_functions failed", __func__ );
        ret = -3;
        goto dbus_client_filter_cleanup;
    }

    dbus_connection_set_dispatch_status_function( connection, dbus_client_handle_dispatch_status, NULL, NULL );
    
    return 0;

dbus_client_filter_cleanup:
    dbus_connection_remove_filter (connection, dbus_client_filter, NULL);
dbus_client_get_cleanup:
    dbus_connection_unref (connection);
dbus_client_error_cleanup:
    dbus_error_free (&error);

    return ret;
}

void dbus_client_deinit( void )
{
    log_debug( "%s(): enter", __func__ );
    
    dbus_connection_remove_filter (connection, dbus_client_filter, NULL);
    dbus_connection_unref (connection);
    dbus_connection_unref (connection_blk);
    dbus_error_free (&error);
    dbus_shutdown();
}

int dbus_client_register_client( dbus_client_t *dbus_client, char * const path, char * const name )
{
    int ret = 0;

    if( dbus_client == NULL || path == NULL || name == NULL )
    {
        log_error( "%s(): invalid arguments dbus_client(0x%X), path(0x%X), name(0x%X)", __func__, dbus_client, path, name );
        return -1;
    }

    dbus_client->path = strdup( path );
    if( dbus_client->path == NULL )
    {
        log_error( "%s(): stdup(dbus_client->path) failed", __func__ );
        return -2;
    }

    dbus_client->name = strdup( name );
    if( dbus_client->name == NULL )
    {
        log_error( "%s(): stdup(dbus_client->path) failed", __func__ );
        ret = -3;
        goto dbus_client_register_client_path_cleanup;
    }

    ret = map_init( &dbus_client->interfaces, sizeof( char * ), sizeof( dbus_client_interface_t ), name_compare );
    if( ret < 0 )
    {
        log_error( "%s(): map_init(dbus_client->interfaces) failed with: %d", __func__, ret );
        ret = -4;
        goto dbus_client_register_client_name_cleanup;
    }

    ret = dbus_client_register_interface( dbus_client, name );
    if( ret < 0 )
    {
        log_error( "%s(): dbus_client_register_interface(%s) failed with: %d", __func__, ret );
        ret = -5;
        goto dbus_client_register_client_interfaces_map_cleanup;
    }

    ret = dbus_connection_register_object_path( connection, path, &dbus_client_vtable, dbus_client );
    if( ret == FALSE )
    {
        //TODO - add more detailed error logging (dbus_connection_try_register_object_path)
        log_error( "%s(): dbus_connection_register_object_path failed", __func__ );
        ret = -6;
        goto dbus_client_register_client_register_interface_cleanup;
    }  

    return 0;

dbus_client_register_client_register_interface_cleanup:
    dbus_client_unregister_interface( dbus_client, name );
dbus_client_register_client_interfaces_map_cleanup:
    map_deinit( &dbus_client->interfaces );
dbus_client_register_client_name_cleanup:
    free( dbus_client->name );
dbus_client_register_client_path_cleanup:
    free( dbus_client->path );
    
    return ret;
}

int dbus_client_unregister_client( dbus_client_t *dbus_client )
{
    dbus_client_unregister_interface( dbus_client, dbus_client->name );
    free( dbus_client->path );
    free( dbus_client->name );
    map_deinit( &dbus_client->interfaces );

    return 0;
}

int dbus_client_register_interface( dbus_client_t *dbus_client, char * const name )
{    
    int ret = 0;

    dbus_client_interface_t interface;

    char *interface_name = NULL;

    if( dbus_client == NULL || name == NULL )
    {
        log_error( "%s(): invalid argument", __func__ );
        return -1;
    }

    ret = map_init( &interface.signals, sizeof( char * ), sizeof( dbus_client_signal_t ), name_compare );
    if( ret < 0 )
    {
        log_error( "%s(): map_init(interface.signals) failed with: %d", __func__, ret );
        return -2;
    }

    ret = map_init( &interface.replies, sizeof( char * ), sizeof( dbus_client_signal_t ), name_compare );
    if( ret < 0 )
    {
        log_error( "%s(): map_init(interface.replies) failed with: %d", __func__, ret );
        map_deinit( &interface.signals );
        return -3;
    }

    interface_name = strdup( name );
    if( interface_name == NULL )
    {
        log_error( "%s(): strdup(interface_name) failed", __func__ );
        map_deinit( &interface.signals );
        map_deinit( &interface.replies );
        return -4;
    }
    
    ret = map_insert( &dbus_client->interfaces, &interface_name, &interface );
    if( ret != 0 )
    {
        log_error( "%s(): map_insert failed with: %d", __func__, ret );
        free( interface_name );
        map_deinit( &interface.signals );
        map_deinit( &interface.replies );
        return -4;
    }

    return ret;
}

int dbus_client_unregister_interface( dbus_client_t *dbus_client, char * const name )
{
    if( dbus_client == NULL || name == NULL )
    {
        log_error( "%s(): invalid arguments", __func__ );
        return -1;
    }

    dbus_client_interface_t *interface = map_find( &dbus_client->interfaces, (void *) name );
    if( interface == NULL )
    {
        log_error( "%s(): unable to find %s interface in %s client", __func__, name, dbus_client->name );
        return -2;
    }


    char **key = map_find_key( &dbus_client->interfaces, (void *)name );
    if( key == NULL )
    {
        log_error( "%s(): unable to find %s key", __func__, name );
        return -3;
    }

    int ret = map_remove( &dbus_client->interfaces, (void *)name );
    if( ret < 0 )
    {
        log_error( "%s(): map_remove failed with: %d", __func__, ret );
        ret = -4;
    }
    else
    {
        ret = 0;
    }

    free( *key );
    
    map_deinit( &interface->signals );
    map_deinit( &interface->replies );

    return ret;
}

int dbus_client_register_signal( dbus_client_t *dbus_client, char * const name, dbus_client_signal_handler_f handler, void *data )
{
    return dbus_client_register_signal_on_interface( dbus_client, dbus_client->name, name, handler, data );
}

int dbus_client_register_signal_on_interface( dbus_client_t *dbus_client, char * const ifce_name, char * const name, dbus_client_signal_handler_f handler, void *data )
{
    int ret = 0;

    if( dbus_client == NULL || name == NULL || handler == NULL )
    {
        log_error( "%s(): invalid arguments dbus_client(0x%X), name(0x%X), handler(0x%X)", __func__, dbus_client, name, handler );
        return -1;
    }

    dbus_client_interface_t *interface = map_find( &dbus_client->interfaces, ifce_name );
    if( interface == NULL )
    {
        log_error( "%s(): unable to find %s interface in %s client", __func__, dbus_client->name, ifce_name );
        return -2;
    }

    dbus_client_signal_t new_signal;
    new_signal.signal_handler = handler;
    new_signal.data = data;

    char *signal_name = strdup( name );
    if( signal_name == NULL )
    {
        log_error( "%s(): strdup(signal_name) failed", __func__ );
        return -3;
    }

    ret = map_insert( &interface->signals, (void *)&signal_name, (void *)&new_signal );
    if( ret < 0 )
    {
        log_error( "%s(): map_insert failed with: %d", __func__, ret );
        ret = -4;
        goto dbus_client_register_signal_name_cleanup;
    }

    char *rule = alloca( strlen( "type='signal',path='',member=''" ) + strlen( dbus_client->path ) + strlen( name ) + 1 /*\0*/ );
    sprintf( rule, "type='signal',path='%s',member='%s'", dbus_client->path, name );

    dbus_bus_add_match( connection, rule, &error );
    if( dbus_error_is_set( &error ) )
    {
        log_error( "%s(): dbus_bus_add_match failed with: %s", __func__, error.message );
        ret = -5;
        goto dbus_client_register_signal_map_cleanup;
    }

    return 0;

dbus_client_register_signal_map_cleanup:
    map_remove( &interface->signals, signal_name );
dbus_client_register_signal_name_cleanup:
    free( signal_name );

    return ret;

}

int dbus_client_unregister_signal( dbus_client_t *dbus_client, char * const name )
{
    return dbus_client_unregister_signal_on_interface( dbus_client, dbus_client->name, name );
}

int dbus_client_unregister_signal_on_interface( dbus_client_t *dbus_client, char * const ifce_name, char * const name )
{
    int ret = 0;

    if( dbus_client == NULL || name == NULL )
    {
        log_error( "%s(): invalid arguments dbus_client(0x%X), name(0x%X)", __func__, dbus_client, name );
        return -1;
    }

    dbus_client_interface_t *interface = map_find( &dbus_client->interfaces, ifce_name );
    if( interface == NULL )
    {
        log_error( "%s(): unable to find %s interface in %s client", __func__, dbus_client->name, ifce_name );
        return -2;
    }

    dbus_client_signal_t *signal = map_find( &interface->signals, name );
    if( signal == NULL )
    {
        log_error( "%s(): unable to find %s signal in registered signals list", __func__, name );
        return -3;
    }

    char **key = map_find_key( &interface->signals, name );
    if( key == NULL )
    {
        log_error( "%s(): unable to find %s key", __func__, name );
        return -4;
    }

    ret = map_remove( &interface->signals, name );
    if( ret < 0 )
    {
        log_error( "%s(): map_remove failed with: %d", __func__, ret );
        ret = -5;
    }
    else
    {
        ret = 0;
    }

    free( *key );

    char *rule = alloca( strlen( "type='signal',path='',member=''" ) + strlen( dbus_client->path ) + strlen( name ) + 1 /*\0*/ );
    sprintf( rule, "type='signal',path='%s',member='%s'", dbus_client->path, name );

    dbus_bus_remove_match( connection, rule, &error );
    if( dbus_error_is_set( &error ) )
    {
        log_error( "%s(): dbus_bus_remove_match with: %s", __func__, error.message );
        ret = -5;
    }

    return ret;
}

int dbus_client_get_signal_args( dbus_client_t *dbus_client, char * const name, const char * format, ... )
{
    int ret = 0;
    va_list va;

    va_start( va, format );
    ret = _dbus_client_get_signal_args_on_interface( dbus_client, dbus_client->name, name, format, va );
    va_end( va );

    return ret;
}

int dbus_client_get_signal_args_on_interface( dbus_client_t *dbus_client, char * const ifce_name, char * const name, const char * format, ... )
{
    int ret = 0;
    va_list va;

    va_start( va, format );
    ret = _dbus_client_get_signal_args_on_interface( dbus_client, ifce_name, name, format, va );
    va_end( va );

    return ret;
}

static int _dbus_client_get_signal_args_on_interface( dbus_client_t *dbus_client, char * const ifce_name, char * const name, const char * format, va_list va )
{
    int ret = 0;
    DBusMessageIter iter;

    if( dbus_client == NULL || name == NULL )
    {
        log_error( "%s(): invalid arguments", __func__ );
        return -1;
    }

    dbus_client_interface_t *interface = map_find( &dbus_client->interfaces, ifce_name );
    if( interface == NULL )
    {
        log_error( "%s(): unable to find %s interface in %s client", __func__, dbus_client->name, ifce_name );
        return -2;
    }

    dbus_client_signal_t *signal = map_find( &interface->signals, (void *)name );
    if( signal == NULL )
    {
        log_error( "%s(): unable to find %s signal in registered signals list", __func__, name );
        return -3;
    }
    
    ret = dbus_message_iter_init( signal->message, &iter );
    if( ret == FALSE )
    {
        log_error( "%s(): dbus_message_iter_init failed message has no arguments.", __func__ );
        return -4;
    }

    ret = dbus_common_parse_message_args( &iter, format, va );
    if( ret != 0 )
    {
        log_error( "%s(): dbus_common_parse_message_args failed with: %d", __func__, ret );
        return -5;
    }

    return 0;
}

int dbus_client_register_reply( dbus_client_t *dbus_client, char * const name, dbus_client_reply_handler_f handler )
{
    return dbus_client_register_reply_on_interface( dbus_client, dbus_client->name, name, handler );
}

int dbus_client_register_reply_on_interface( dbus_client_t *dbus_client, char * const ifce_name, char * const name, dbus_client_reply_handler_f handler )
{
    int ret = 0;

    if( dbus_client == NULL || name == NULL || handler == NULL )
    {
        log_error( "%s(): invalid arguments dbus_client(0x%X), name(0x%X), handler(0x%X)", __func__, dbus_client, name, handler );
        return -1;
    }

    dbus_client_interface_t *interface = map_find( &dbus_client->interfaces, ifce_name );
    if( interface == NULL )
    {
        log_error( "%s(): unable to find %s interface in %s client", __func__, dbus_client->name, ifce_name );
        return -2;
    }

    dbus_client_reply_t new_reply;
    new_reply.reply_handler = handler;

    char *reply_name = strdup( name );
    if( reply_name == NULL )
    {
        log_error( "%s(): strdup(signal_name) failed", __func__ );
        return -3;
    }

    ret = map_insert( &interface->replies, (void *)&reply_name, (void *)&new_reply );
    if( ret < 0 )
    {
        log_error( "%s(): map_insert failed with: %d", __func__, ret );
        ret = -4;
        goto dbus_client_register_reply_name_cleanup;
    }

    return 0;

dbus_client_register_reply_name_cleanup:
    free( reply_name );

    return ret;
}

int dbus_client_unregister_reply( dbus_client_t *dbus_client, char * const name )
{
    return dbus_client_unregister_reply_on_interface( dbus_client, dbus_client->name, name );
}

int dbus_client_unregister_reply_on_interface( dbus_client_t *dbus_client, char * const ifce_name, char * const name )
{
    int ret = 0;

    if( dbus_client == NULL || name == NULL )
    {
        log_error( "%s(): invalid arguments dbus_client(0x%X), name(0x%X)", __func__, dbus_client, name );
        return -1;
    }

    dbus_client_interface_t *interface = map_find( &dbus_client->interfaces, ifce_name );
    if( interface == NULL )
    {
        log_error( "%s(): unable to find %s interface in %s client", __func__, dbus_client->name, ifce_name );
        return -2;
    }

    dbus_client_reply_t *reply = map_find( &interface->replies, name );
    if( signal == NULL )
    {
        log_error( "%s(): unable to find %s reply in registered replies list", __func__, name );
        return -3;
    }

    char **key = map_find_key( &interface->replies, name );
    if( key == NULL )
    {
        log_error( "%s(): unable to find %s key", __func__, name );
        return -4;
    }

    ret = map_remove( &interface->replies, name );
    if( ret < 0 )
    {
        log_error( "%s(): map_remove failed with: %d", __func__, ret );
        ret = -5;
    }
    else
    {
        ret = 0;
    }

    free( *key );

    return 0;
}

int dbus_client_get_reply_args( dbus_client_t *dbus_client, uint32_t *id, char * const name, const char * format, ... )
{
    int ret = 0;
    va_list va;

    va_start( va, format );
    ret = _dbus_client_get_reply_args_on_interface( dbus_client, id, dbus_client->name, name, format, va ); 
    va_end( va );

    return ret;
}

int dbus_client_get_reply_args_on_interface( dbus_client_t *dbus_client, uint32_t *id, char * const ifce_name, char * const name, const char * format, ... )
{
    int ret = 0;
    va_list va;

    va_start( va, format );
    ret = _dbus_client_get_reply_args_on_interface( dbus_client, id, ifce_name, name, format, va ); 
    va_end( va );

    return ret;
}

static int _dbus_client_get_reply_args_on_interface( dbus_client_t *dbus_client, uint32_t *id, char * const ifce_name, char * const name, const char * format, va_list va )
{
    int ret = 0;
    DBusMessageIter iter;

    if( dbus_client == NULL || name == NULL )
    {
        log_error( "%s(): invalid arguments", __func__ );
        return -1;
    }
    
    dbus_client_interface_t *interface = map_find( &dbus_client->interfaces, ifce_name );
    if( interface == NULL )
    {
        log_error( "%s(): unable to find %s interface in %s client", __func__, dbus_client->name, ifce_name );
        return -2;
    }

    dbus_client_reply_t *reply = map_find( &interface->replies, (void *)name );
    if( signal == NULL )
    {
        log_error( "%s(): unable to find %s signal in registered signals list", __func__, name );
        return -3;
    }

    if( id != NULL )
        *id = dbus_message_get_reply_serial(reply->message);

    ret = dbus_message_iter_init( reply->message, &iter );
    if( ret == FALSE )
    {
        log_error( "%s(): dbus_message_iter_init failed message has no arguments.", __func__ );
        return -4;
    }

    ret = dbus_common_parse_message_args( &iter, format, va );
    if( ret != 0 )
    {
        log_error( "%s(): dbus_common_parse_message_args failed with: %d", __func__, ret );
        return -5;
    }

    return 0;
}

int dbus_client_call_method( dbus_client_t *dbus_client, uint32_t *id, char * const name, const char * format, ... )
{
    int ret = 0;
    va_list va;
    
    va_start( va, format );
    ret = _dbus_client_call_method_on_interface( dbus_client, dbus_no_block, id, dbus_client->name, name, format, va );
    va_end( va );

    return ret;
}

int dbus_client_call_method_on_interface( dbus_client_t *dbus_client, uint32_t *id, char * const ifce_name, char * const name, const char *format, ... )
{
    int ret = 0;
    va_list va;
    
    va_start( va, format );
    ret = _dbus_client_call_method_on_interface( dbus_client, dbus_no_block, id, ifce_name, name, format, va );
    va_end( va );

    return ret;
}

int dbus_client_call_methodi_and_block( dbus_client_t *dbus_client, uint32_t *id, char * const name, const char * format, ... )
{
    int ret = 0;
    va_list va;
    
    va_start( va, format );
    ret = _dbus_client_call_method_on_interface( dbus_client, dbus_block, id, dbus_client->name, name, format, va );
    va_end( va );

    return ret;
}

int dbus_client_call_method_on_interface_and_block( dbus_client_t *dbus_client, uint32_t *id, char * const ifce_name, char * const name, const char *format, ... )
{
    int ret = 0;
    va_list va;
    
    va_start( va, format );
    ret = _dbus_client_call_method_on_interface( dbus_client, dbus_block, id, ifce_name, name, format, va );
    va_end( va );

    return ret;
}


static int _dbus_client_call_method_on_interface( dbus_client_t *dbus_client, dbus_block_type_t block, uint32_t *id, char * const ifce_name, char * const name, const char *format, va_list va )
{
    int ret = 0;
    DBusMessageIter iter;

    DBusMessage *message = dbus_message_new_method_call( NULL, dbus_client->path, dbus_client->name, name );
    if( message == NULL )
    {
        log_error( "%s(): dbus_message_new_method_call failed", __func__ );
        return -1;
    }
   
    if( format != NULL )
    { 
        dbus_message_iter_init_append( message, &iter );

        ret = dbus_common_build_message_args( &iter, NULL, DBUS_TYPE_INVALID, format, va );
        if( ret != 0 )
        {
            log_error( "%s(): dbus_common_build_message_args failed with: %d", __func__, ret );
            return -2;
        }
    }
    
    dbus_message_set_destination( message, dbus_client->name );
    dbus_message_set_no_reply( message, TRUE );
    dbus_message_set_interface( message, ifce_name );

    ret =  dbus_connection_send( connection, message, id );
    if( ret == FALSE )
    {
        log_error( "%s(): dbus_connection_send", __func__ );
        dbus_message_unref( message );
        return -3;
    }

    dbus_message_unref( message );

    return 0;
}

int dbus_client_call_method_with_reply( dbus_client_t *dbus_client, uint32_t *id, char * const name, const char *format, ... )
{
    int ret = 0;
    va_list va;

    va_start( va, format );
    ret = _dbus_client_call_method_with_reply_on_interface( dbus_client, dbus_no_block, id, dbus_client->name, name, format, va );
    va_end( va );

    return ret;

}

int dbus_client_call_method_with_reply_on_interface( dbus_client_t *dbus_client, uint32_t *id, char * const ifce_name, char * const name, const char *format, ... )
{
    int ret = 0;
    va_list va;
    
    va_start( va, format );
    ret = _dbus_client_call_method_with_reply_on_interface( dbus_client, dbus_no_block, id, ifce_name, name, format, va );
    va_end( va );

    return ret;
}

int dbus_client_call_method_with_reply_and_block( dbus_client_t *dbus_client, uint32_t *id, char * const name, const char *format, ... )
{
    int ret = 0;
    va_list va;

    va_start( va, format );
    ret = _dbus_client_call_method_with_reply_on_interface( dbus_client, dbus_block, id, dbus_client->name, name, format, va );
    va_end( va );

    return ret;

}

int dbus_client_call_method_with_reply_on_interface_and_block( dbus_client_t *dbus_client, uint32_t *id, char * const ifce_name, char * const name, const char *format, ... )
{
    int ret = 0;
    va_list va;
    
    va_start( va, format );
    ret = _dbus_client_call_method_with_reply_on_interface( dbus_client, dbus_block, id, ifce_name, name, format, va );
    va_end( va );

    return ret;
}

static int _dbus_client_call_method_with_reply_on_interface( dbus_client_t *dbus_client, dbus_block_type_t block, uint32_t *id, char * const ifce_name, char * const name, const char *format, va_list va )
{
    int ret = 0;
    DBusMessageIter iter;
    
    if( dbus_client == NULL || ifce_name == NULL || name == NULL )
    {
        log_error( "%s(): invalid arguments", __func__ );
        return -1;
    }

    dbus_client_interface_t *interface = map_find( &dbus_client->interfaces, ifce_name );
    if( interface == NULL )
    {
        log_error( "%s(): unable to find %s interface in %s client", __func__, dbus_client->name, ifce_name );
        return -2;
    }

    dbus_client_reply_t *reply = map_find( &interface->replies, name );
    if( reply == NULL )
    {
        log_error( "%s(): unable to find reply for message: %s in registered replies", __func__, name );
        return -3;
    }
    
    DBusMessage *message = dbus_message_new_method_call( NULL, dbus_client->path, dbus_client->name, name );
    if( message == NULL )
    {
        log_error( "%s(): dbus_message_new_method_call failed", __func__ );
        return -2;
    }

    dbus_message_iter_init_append( message, &iter );
    
    if( format != NULL )
    {
        ret = dbus_common_build_message_args( &iter, NULL, DBUS_TYPE_INVALID, format, va );
        if( ret != 0 )
        {
            log_error( "%s(): dbus_common_build_message_args failed with: %d", __func__, ret );
            dbus_message_unref( message );
            return -3;
        }
    }

    dbus_message_set_destination( message, dbus_client->name );
    dbus_message_set_interface( message, ifce_name );
    
    if( block == dbus_no_block )
    {
        DBusPendingCall *pending = NULL;
        ret =  dbus_connection_send_with_reply( connection, message, &pending, DBUS_TIMEOUT_INFINITE );
        if( ret == FALSE )
        {
            log_error( "%s(): dbus_connection_send_with_reply failed", __func__ );
            dbus_message_unref( message );
            return -4;
        }

        ret = dbus_pending_call_set_notify( pending, dbus_client_reply_handler, (void *)reply, NULL );
        if( ret == FALSE )
        {
            log_error( "%s(): dbus_pending_call_set_notify failed", __func__ );
            dbus_message_unref( message );
            dbus_pending_call_cancel( pending );
            dbus_pending_call_unref( pending );
            return -5;
        }
    }
    else
    {
        reply->message = dbus_connection_send_with_reply_and_block( connection_blk, message, DBUS_TIMEOUT_INFINITE, &error );
        if( reply->message == NULL )
        {
            log_error( "%s(): dbus_connection_send_with_reply_and_block failed: %s", __func__, error.message );
            dbus_message_unref( message );
            return -6;
        }
    }

    if( id != NULL )
        *id = dbus_message_get_serial( message );
    dbus_message_unref( message );

    return 0;
}

static DBusHandlerResult dbus_client_filter( DBusConnection *connection, DBusMessage *message, void *data )
{
    log_debug( "%s(): enter", __func__ );

    if( dbus_message_is_signal( message, DBUS_INTERFACE_LOCAL, "Disconnected" ) )
    {
        kill( getpid(), SIGINT );
        return DBUS_HANDLER_RESULT_HANDLED;
    }
    else
    {
        return DBUS_HANDLER_RESULT_NOT_YET_HANDLED;
    }
}

static dbus_bool_t dbus_client_add_watch( DBusWatch * watch, void * data )
{
    log_debug( "%s(): enter", __func__ );

    int ret = io_add_watch_data( dbus_watch_get_unix_fd(watch), &dbus_client_io_config, (void *)watch );
    if( ret )
    {
        log_error( "%s(): io_add_watch_data failed with: %d", __func__, ret );
        return FALSE;
    }
    
    return TRUE;
}

static void dbus_client_remove_watch( DBusWatch * watch, void * data )
{
    log_debug( "%s(): enter", __func__ );
    io_del_watch_data( (void *)watch );
}

static void dbus_client_toggle_watch( DBusWatch * watch, void * data )
{
    log_debug( "%s(): enter", __func__ );
    dbus_client_remove_watch( watch, data );
    
    if( dbus_watch_get_enabled( watch ) == TRUE )
    {
        dbus_client_add_watch( watch, data );
    }
}

static void dbus_client_handle_dispatch_status( DBusConnection *connection, DBusDispatchStatus status, void *data )
{
    if( status == DBUS_DISPATCH_DATA_REMAINS )
    {
        log_debug( "%s(): DBUS_DISPATCH_DATA_REMAINS", __func__ );
        while( dbus_connection_get_dispatch_status(connection) == DBUS_DISPATCH_DATA_REMAINS )
        {
            dbus_connection_dispatch(connection);
        }
    }
    else if( status == DBUS_DISPATCH_COMPLETE )
    {
        log_debug( "%s(): DBUS_DISPATCH_COMPLETE", __func__ );
    }
}

static void dbus_client_unregister_handler( DBusConnection  *connection, void *data )
{
    log_debug( "%s(): enter", __func__ );
}

static DBusHandlerResult dbus_client_message_handler( DBusConnection *connection, DBusMessage *message, void *data )
{
    int ret;
    log_debug( "%s(): enter", __func__ );

    dbus_client_t *dbus_client = (dbus_client_t *)data;
    if( dbus_client == NULL )
    {
        log_error( "%s(): invalid arguments connection(0x%X), message(0x%X), data(0x%X)", __func__, connection, message, data );
        return DBUS_HANDLER_RESULT_HANDLED;
    }

    const char *ifce_name = dbus_message_get_interface( message );
    if( ifce_name == NULL )
    {
        log_error( "%s(): dbus_message_get_interface returned NULL", __func__ );
        ifce_name = dbus_client->name;
    }

    dbus_client_interface_t *interface = map_find( &dbus_client->interfaces, (char *)ifce_name );
    if( interface == NULL )
    {
        log_error( "%s(): unable to find %s interface in %s client", __func__, ifce_name, dbus_client->name );
        return DBUS_HANDLER_RESULT_HANDLED;
    }

    const char *member = dbus_message_get_member( message );
    if( member == NULL )
    {
        log_error( "%s(): dbus_message_get_member failed", __func__ );
        return DBUS_HANDLER_RESULT_NEED_MEMORY;
    }

    if( dbus_message_is_signal( message, ifce_name, member ) == TRUE )
    {
        log_debug( "%s(): signal received", __func__ );
        dbus_client_signal_t *signal = map_find( &interface->signals, (void *)member );
        if( signal == NULL )
        {
            log_error( "%s(): map_find %s signal failed", __func__, member );
            return DBUS_HANDLER_RESULT_HANDLED;
        }

        signal->message = message;
        
        ret = signal->signal_handler( signal->data );
        if( ret < 0 )
        {
            log_error( "%s(): (%s) signal_handler failed with: %d", __func__, member, ret );
        }

        signal->message = NULL;
    }
    else
    {
        log_debug( "%s(): reply received", __func__ );
    }
   
    return DBUS_HANDLER_RESULT_HANDLED;
}

static void dbus_client_reply_handler( DBusPendingCall *pending, void *data )
{
    int ret = 0;

    log_debug( "%s(): enter", __func__ );

    dbus_client_reply_t *reply = (dbus_client_reply_t *)data;

    DBusMessage *message = dbus_pending_call_steal_reply( pending );
    if( message == NULL )
    {
        log_error( "%s(): dbus_pending_call_steal_reply failed", __func__ );
        dbus_pending_call_cancel( pending );
        return;
    }

    reply->message = message;
    ret = reply->reply_handler( NULL );
    if( ret < 0 )
    {
        log_error( "%s(): reply_handler failed", __func__ );
        return;
    }

    dbus_message_unref( message );
    
    reply->message = NULL;
    
    dbus_pending_call_unref( pending );
}

static int dbus_client_read_cb( int fd, void *data )
{
    log_debug( "%s(): enter", __func__ );
    
    DBusWatch *watch = (DBusWatch *)data;
    dbus_bool_t ret = dbus_watch_handle(watch, DBUS_WATCH_READABLE);
    if( ret == FALSE )
    {
        log_error( "%s(): dbus_watch_handle failed", __func__ );
        return -1;
    }

    dbus_client_handle_dispatch_status( connection, DBUS_DISPATCH_DATA_REMAINS, NULL );

    return 0;
}

static int dbus_client_timeout_cb( int fd, void *data )
{
    log_debug( "%s(): enter", __func__ );
    
    return 0;
}

static int name_compare( const void *name_1, const void *name_2 )
{
    return strcmp( *((const char **)name_1), (const char *)name_2 );
}

