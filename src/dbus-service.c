/*
 * ----------------------------------------------------------------------------
 * "THE BEER-WARE LICENSE" (Revision 42):
 * <nowacki.jakub@gmail.com> wrote this file. As long as you retain this notice you
 * can do whatever you want with this stuff. If we meet some day, and you think
 * this stuff is worth it, you can buy me a beer in return Jakub Nowacki
 * ----------------------------------------------------------------------------
 */

#include <string.h>
#include <signal.h>
#include <stdarg.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

#include <dbus/dbus.h>

#include <ctools/io.h>
#include <ctools/log.h>

#include "dbus-common.h"
#include "dbus-service.h"

typedef struct dbus_service_current_method_s
{
    DBusMessage *message;
    dbus_bool_t reply_sent;
    dbus_bool_t error_sent;
} dbus_service_current_method_t;

typedef struct dbus_service_method_s
{
    dbus_service_method_handler_f method_handler;
    dbus_service_current_method_t current_method;
} dbus_service_method_t;

//Static functions -- Start
static DBusHandlerResult dbus_service_filter( DBusConnection *connection, DBusMessage *message, void *user_data );

static dbus_bool_t dbus_service_add_watch( DBusWatch * watch, void * data );
static void dbus_service_remove_watch( DBusWatch * watch, void * data );
static void dbus_service_toggle_watch( DBusWatch * watch, void * data );

static void dbus_service_handle_dispatch_status( DBusConnection *connection, DBusDispatchStatus status, void *data );

static void dbus_service_unregister_handler( DBusConnection  *connection, void *data );
static DBusHandlerResult dbus_service_message_handler( DBusConnection *connection, DBusMessage *message, void *data );
static int dbus_service_read_cb( int fd, void *data );
static int dbus_service_timeout_cb( int fd, void *data );

static int name_compare( const void *name_1, const void *name_2 );

static map_t* dbus_service_get_interface( dbus_service_t *dbus_service, char * const name );

static int _dbus_service_get_method_args_on_interface( dbus_service_t *dbus_service, char * const ifce_name, char * const name, char * const format, va_list va );

static int _dbus_service_send_reply_on_interface( dbus_service_t *dbus_service, char * const ifce_name, char * const name, char * const format , va_list va );
static int _dbus_service_send_signal_on_interface( dbus_service_t *dbus_service, char * const ifce_name, char * const name, char * const format, va_list va );

//Static functions -- End

//Static variables -- Start
static DBusConnection *connection;
static DBusError error;

static DBusObjectPathVTable dbus_service_vtable = {
    .unregister_function = dbus_service_unregister_handler,
    .message_function = dbus_service_message_handler,
    NULL,
};

static io_config_t dbus_service_io_config = {
    .cb = {
        .read_cb = dbus_service_read_cb,
        .write_cb = NULL,
        .timeout_cb = dbus_service_timeout_cb,
    },
    .timeout = {
        .tv_sec = 1,
        .tv_nsec = 0,
    }
};
//Static variables -- End

int dbus_service_init( dbus_bus_type_t bus_type )
{
    int ret = 0;

    log_debug( "%s(): enter", __func__ );
    
    dbus_error_init( &error );

    connection = dbus_bus_get( dbus_common_map_bus_type( bus_type ), &error );
    if( connection == NULL )
    {
        log_error( "%s(): dbus_bus_get failed with: %s", __func__, error.message );
        ret = -1;
        goto dbus_service_error_cleanup;
    }

    dbus_connection_set_exit_on_disconnect( connection, FALSE );

    ret = dbus_connection_add_filter( connection, dbus_service_filter, NULL, NULL );
    if( ret == FALSE )
    {
        log_error( "%s(): dbus_connection_add_filter failed", __func__ );
        ret = -2;
        goto dbus_service_get_cleanup;
    }
   
    ret = dbus_connection_set_watch_functions( connection, dbus_service_add_watch, dbus_service_remove_watch, dbus_service_toggle_watch, NULL, NULL );
    if( ret == FALSE )
    {
        log_error( "%s(): dbus_connection_set_watch_functions failed", __func__ );
        ret = -3;
        goto dbus_service_filter_cleanup;
    }

    dbus_connection_set_dispatch_status_function( connection, dbus_service_handle_dispatch_status, NULL, NULL );
    
    return 0;

dbus_service_filter_cleanup:
    dbus_connection_remove_filter (connection, dbus_service_filter, NULL);
dbus_service_get_cleanup:
    dbus_connection_unref (connection);
dbus_service_error_cleanup:
    dbus_error_free (&error);

    return ret;
}

void dbus_service_deinit( void )
{
    log_debug( "%s(): enter", __func__ );
    
    dbus_connection_remove_filter (connection, dbus_service_filter, NULL);
    dbus_connection_unref (connection);
    dbus_error_free (&error);
    dbus_shutdown();
}

int dbus_service_register_service( dbus_service_t *dbus_service, char * const path, char * const name )
{
    int ret = 0;
    
    log_debug( "%s(): enter", __func__ );

    ret = map_init( &dbus_service->interfaces, sizeof( char * ), sizeof( map_t ), name_compare );
    if( ret != 0 )
    {
        log_error( "%s(): map_init failed with: %d", __func__, ret );
        return -1;
    }

    dbus_service->path = strdup( path );
    if( dbus_service->path == NULL )
    {
        log_error( "%s(): strdup(dbus_service->path) failed", __func__ );
        ret = -2;
        goto dbus_service_register_service_cleanup;
    }
    
    dbus_service->name = strdup( name );
    if( dbus_service->name == NULL )
    {
        log_error( "%s(): strdup(dbus_service->name) failed", __func__ );
        ret = -3;
        goto dbus_service_register_service_path_cleanup;
    }
    
    ret = dbus_connection_register_object_path( connection, path, &dbus_service_vtable, dbus_service );
    if( ret == FALSE )
    {
        log_error( "%s(): dbus_connection_register_object_path failed", __func__ );
        ret = -4;
        goto dbus_service_register_service_name_cleanup;
    }

	ret = dbus_bus_request_name( connection, name, 0, &error );
    if( ret == FALSE )
    {
        log_error( "%s(): dbus_bus_request_name failed with: %s", __func__, error.message );
        ret = -5;
        goto dbus_service_register_service_register_object_path_cleanup;
    }

    ret = dbus_service_register_interface( dbus_service, dbus_service->name );
    if( ret != 0 )
    {
        log_error( "%s(): dbus_service_register_interface failed with: %d", __func__, ret );
        ret = -6;
        goto dbus_service_register_service_dbus_bus_request_name_cleanup;
    }
    
    return 0;  

dbus_service_register_service_dbus_bus_request_name_cleanup:
    dbus_bus_release_name( connection, name, NULL );
dbus_service_register_service_register_object_path_cleanup:
    dbus_connection_unregister_object_path( connection, dbus_service->path );
dbus_service_register_service_name_cleanup:
    free(dbus_service->name);
dbus_service_register_service_path_cleanup:
    free(dbus_service->path);
dbus_service_register_service_cleanup:
    map_deinit( &dbus_service->interfaces );

    return ret;

}

int dbus_service_unregister_service( dbus_service_t *dbus_service )
{
    int ret = 0;

    if( dbus_service == NULL )
    {
        log_error( "%s(): invalid argument", __func__ );
        return -1;
    }

    ret = dbus_bus_release_name( connection, dbus_service->name, &error );
    if( ret  < 0 )
    {
        log_error( "%s(): dbus_bus_release_name failed with: %s", __func__, error.message );
    }

    dbus_connection_unregister_object_path( connection, dbus_service->path );

    ret = dbus_service_unregister_interface( dbus_service, dbus_service->name );
    if( ret != 0 )
    {
        log_error( "%s(): dbus_service_unregister_interfacefailed with: %s", __func__, ret );
    }

    map_deinit( &dbus_service->interfaces );
    free( dbus_service->path );
    free( dbus_service->name );

    return 0;
}

int dbus_service_register_interface( dbus_service_t *dbus_service, char * const name )
{
    int ret = 0;
    map_t new_map;
    char *interface_name = NULL;

    if( dbus_service == NULL || name == NULL )
    {
        log_error( "%s(): invalid argument", __func__ );
        return -1;
    }

    ret = map_init( &new_map, sizeof( char * ), sizeof( dbus_service_method_t ), name_compare );
    if( ret != 0 )
    {
        log_error( "%s(): map_init failed with: %d", __func__, ret );
        return -2;
    }

    interface_name = strdup( name );
    if( interface_name == NULL )
    {
        log_error( "%s(): strdup(interface_name) failed", __func__ );
        map_deinit( &new_map );
        return -3;
    }

    ret = map_insert( &dbus_service->interfaces, &interface_name, &new_map );
    if( ret != 0 )
    {
        log_error( "%s(): map_insert failed with: %d", __func__, ret );
        free( interface_name );
        map_deinit( &new_map );
        return -4;
    }
    
    return 0;
}

int dbus_service_unregister_interface( dbus_service_t *dbus_service, char * const name )
{    
    if( dbus_service == NULL || name == NULL )
    {
        log_error( "%s(): invalid arguments", __func__ );
        return -1;
    }

    map_t *interface = map_find( &dbus_service->interfaces, (void *)name );
    if( interface == NULL )
    {
        log_error( "%s(): unable to find %s interface in %s service", __func__, name, dbus_service->name );
        return -2;
    }

    char **key = map_find_key( &dbus_service->interfaces, (void *)name );
    if( key == NULL )
    {
        log_error( "%s(): unable to find %s key", __func__, name );
        return -3;
    }
    
    int ret = map_remove( &dbus_service->interfaces, (void *)name );
    if( ret < 0 )
    {
        log_error( "%s(): map_remove failed with: %d", __func__, ret );
        ret = -4;
    }
    else
    {
        ret = 0;
    }

    free( *key );
    
    map_deinit( interface );

    return ret;
}

int dbus_service_register_method( dbus_service_t *dbus_service, char * const name, dbus_service_method_handler_f handler )
{
    return dbus_service_register_method_on_interface( dbus_service, dbus_service->name, name, handler );
}

int dbus_service_unregister_method( dbus_service_t *dbus_service, char * const name )
{
    return dbus_service_unregister_method_on_interface( dbus_service, dbus_service->name, name );
}

int dbus_service_register_method_on_interface( dbus_service_t *dbus_service, char * const ifce_name, char * const name, dbus_service_method_handler_f handler )
{
    int ret = 0;

    if( dbus_service == NULL || ifce_name == NULL || name== NULL || handler == NULL )
    {
        log_error( "%s(): invalid arguments dbus_service(0x%X), ifce_name(0x%X), name(0x%X), handler(0x%X)", __func__, dbus_service, ifce_name, name, handler );
        return -1;
    }

    map_t *interface = dbus_service_get_interface( dbus_service, ifce_name );
    if( interface == NULL )
    {
        log_error( "%s(): unable to find %s interface in %s service", __func__, dbus_service->name, ifce_name );
        return -2;
    }
    
    dbus_service_method_t new_method;
    new_method.method_handler = handler;

    char *method_name = strdup(name);
    if( method_name == NULL )
    {
        log_error( "%s(): strdup(method_name) failed", __func__ );
        return -3;
    }
    ret = map_insert( interface, (void *)&method_name, (void *)&new_method );
    if( ret < 0 )
    {
        log_error( "%s(): map_insert failed with: %d", __func__, ret );
        free( method_name );
        return -4;
    }

    return 0;

}

int dbus_service_unregister_method_on_interface( dbus_service_t *dbus_service, char * const ifce_name, char * const name )
{
    if( dbus_service == NULL || ifce_name == NULL || name == NULL )
    {
        log_error( "%s(): invalid arguments", __func__ );
        return -1;
    }
    
    map_t *interface = dbus_service_get_interface( dbus_service, ifce_name );
    if( interface == NULL )
    {
        log_error( "%s(): unable to find %s interface in %s service", __func__, dbus_service->name, ifce_name );
        return -2;
    }

    dbus_service_method_t *method = map_find( interface, (void *)name );
    if( method == NULL )
    {
        log_error( "%s(): unable to find %s method in %s service", __func__, name, dbus_service->name );
        return -3;
    }

    char **key = map_find_key( interface, (void *)name );
    if( key == NULL )
    {
        log_error( "%s(): unable to find %s key", __func__, name );
        return -4;
    }

    int ret = map_remove( interface, (void *)name );
    if( ret < 0 )
    {
        log_error( "%s(): map_remove failed with: %d", __func__, ret );
        ret = -5;
    }
    else
    {
        ret = 0;
    }

    free( *key );

    return ret;
}

int dbus_service_get_method_args( dbus_service_t *dbus_service, char * const name, char * const format, ... )
{
    va_list va;
    int ret = 0;
    va_start(va, format);
    ret = _dbus_service_get_method_args_on_interface( dbus_service, dbus_service->name, name, format, va );
    va_end(va);
    return ret;


}

int dbus_service_get_method_args_on_interface( dbus_service_t *dbus_service, char * const ifce_name, char * const name, char * const format, ... )
{
    va_list va;
    int ret = 0;
    va_start(va, format);
    ret = _dbus_service_get_method_args_on_interface( dbus_service, ifce_name, name, format, va );
    va_end(va);
    return ret;
}

static int _dbus_service_get_method_args_on_interface( dbus_service_t *dbus_service, char * const ifce_name, char * const name, char * const format, va_list va )
{
    uint8_t ret;
    DBusMessageIter iter;

    if( dbus_service == NULL || name == NULL )
    {
        log_error( "%s(): invalid arguments", __func__ );
        return -1;
    }

    map_t *interface = dbus_service_get_interface( dbus_service, ifce_name );
    if( interface == NULL )
    {
        log_error( "%s(): unable to find %s interface in %s service", __func__, ifce_name, dbus_service->name );
        return -2;
    }

    dbus_service_method_t *method = map_find( interface, (void *)name );
    if( method == NULL )
    {
        log_error( "%s(): unable to find %s method in %s interface", __func__, name, ifce_name );
        return -3;
    }

    ret = dbus_message_iter_init( method->current_method.message, &iter );
    if( ret == FALSE )
    {
        log_error( "%s(): dbus_message_iter_init failed - message has no arguments." );
        return -4;
    }

    ret = dbus_common_parse_message_args( &iter, format, va );
    if( ret != 0 )
    {
        log_error( "%s(): dbus_common_parse_message_args failed with: %d", __func__, ret );
        return -5; 
    }

    return 0;
}

int dbus_service_send_reply( dbus_service_t *dbus_service, char * const name, char * const format, ... )
{
    va_list va;
    int ret = 0;
    
    va_start(va, format );
    ret = _dbus_service_send_reply_on_interface( dbus_service, dbus_service->name, name, format, va ); 
    va_end(va);

    return ret;
}

int dbus_service_send_reply_on_interface( dbus_service_t *dbus_service, char * const ifce_name, char * const name, char * const format, ... )
{
    va_list va;
    int ret = 0;
    
    va_start(va, format );
    ret = _dbus_service_send_reply_on_interface( dbus_service, ifce_name, name, format, va );
    va_end(va);

    return ret;
}

static int _dbus_service_send_reply_on_interface( dbus_service_t *dbus_service, char * const ifce_name, char * const name, char * const format, va_list va )
{
    uint8_t ret = 0;
    DBusMessageIter iter;

    if( dbus_service == NULL || name == NULL )
    {
        log_error( "%s(): invalid arguments", __func__ );
        return -1;
    }

    map_t *interface = dbus_service_get_interface( dbus_service, ifce_name );
    if( interface == NULL )
    {
        log_error( "%s(): unable to find %s interface in %s service", __func__, ifce_name, dbus_service->name );
        return -2;
    }

    dbus_service_method_t *method = map_find( interface, (void *)name );
    if( method == NULL )
    {
        log_error( "%s(): unable to find %s method on %s interface", __func__, name, ifce_name );
        return -3;
    }

    DBusMessage *reply = dbus_message_new_method_return( method->current_method.message );
    if( reply == NULL )
    {
        log_error( "%s(): dbus_message_new_method_return failed", __func__ );
        return -4;
    }
    
    dbus_message_iter_init_append( reply, &iter );
    
    ret = dbus_common_build_message_args( &iter, NULL, DBUS_TYPE_INVALID, format, va );
    if( ret != 0 )
    {
        log_error( "%s(): dbus_common_build_message_args failed with: %d", __func__, ret );
        return -5;
    }

    ret = dbus_connection_send( connection, reply, NULL );
    if( ret == FALSE )
    {
        log_error( "%s(): dbus_connection_send failed", __func__ );
        dbus_message_unref(reply);
        return -6;
    }

    dbus_message_unref(reply);

    method->current_method.reply_sent = TRUE;

    return 0;
}


int dbus_service_send_signal( dbus_service_t *dbus_service, char * const name, char * const format, ... )
{
    va_list va;
    int ret = 0;
    
    va_start( va, format );
    ret = _dbus_service_send_signal_on_interface( dbus_service, dbus_service->name, name, format, va );
    va_end( va );

    return ret;
}

int dbus_service_send_signal_on_interface( dbus_service_t *dbus_service, char * const ifce_name, char * const name, char * const format, ... )
{
    va_list va;
    int ret = 0;
    
    va_start( va, format );
    ret = _dbus_service_send_signal_on_interface( dbus_service, ifce_name, name, format, va );
    va_end( va );

    return ret;
}

static int _dbus_service_send_signal_on_interface( dbus_service_t *dbus_service, char * const ifce_name, char * const name, char * const format, va_list va )
{
    uint8_t ret = 0;
    DBusMessageIter iter;

    if( dbus_service == NULL || name == NULL )
    {
        log_error( "%s(): invalid arguments", __func__ );
        return -1;
    }

    DBusMessage *signal = dbus_message_new_signal( dbus_service->path, dbus_service->name, name );
    if( signal == NULL )
    {
        log_error( "%s(): dbus_message_new_signal failed", __func__ );
        return -2;
    }

    ret = dbus_message_set_interface( signal, ifce_name );
    if( ret == FALSE )
    {
        log_error( "%s():  dbus_message_set_interface failed", __func__ );
        dbus_message_unref( signal );
        return -3;
    }
    
    dbus_message_iter_init_append( signal, &iter );
    
    ret = dbus_common_build_message_args( &iter, NULL, DBUS_TYPE_INVALID, format, va );
    if( ret != 0 )
    {
        log_error( "%s(): dbus_common_build_message_args failed with: %d", __func__, ret );
        dbus_message_unref( signal );
        return -4;
    }
    
    ret = dbus_connection_send( connection, signal, NULL );
    if( ret == FALSE )
    {
        log_error( "%s(): dbus_connection_send failed", __func__ );
        dbus_message_unref( signal );
        return -5;
    }

    dbus_message_unref( signal );

    return 0;
}


int dbus_service_send_error( dbus_service_t *dbus_service, char * const name, char * const error_name, char * const error_message )
{
    uint8_t ret = 0;

    if( dbus_service == NULL || name == NULL )
    {
        log_error( "%s(): invalid arguments", __func__ );
        return -1;
    }

    map_t *interface = dbus_service_get_interface( dbus_service, dbus_service->name );
    if( interface == NULL )
    {
        log_error( "%s(): unable to find %s interface in %s service", __func__, dbus_service->name, dbus_service->name );
        return -2;
    }

    dbus_service_method_t *method = map_find( interface, (void *)name );
    if( method == NULL )
    {
        log_error( "%s(): unable to find %s method on %s interface", __func__, name, dbus_service->name );
        return -2;
    }

    DBusMessage *error = dbus_message_new_error( method->current_method.message, error_name, error_message );
    if( error == NULL )
    {
        log_error( "%s(): dbus_message_new_error failed", __func__ );
        return -3;
    }

    ret = dbus_connection_send( connection, error, NULL );
    if( ret == FALSE )
    {
        log_error( "%s(): dbus_connection_send failed", __func__ );
        dbus_message_unref( error );
        return -4;
    }

    dbus_message_unref(error);

    method->current_method.error_sent = TRUE;

    return 0;
}

int dbus_service_send_error_on_interface( dbus_service_t *dbus_service, char * const ifce_name, char * const name, char * const error_name, char * const error_message )
{
    uint8_t ret = 0;

    if( dbus_service == NULL || name == NULL )
    {
        log_error( "%s(): invalid arguments", __func__ );
        return -1;
    }

    map_t *interface = dbus_service_get_interface( dbus_service, ifce_name );
    if( interface == NULL )
    {
        log_error( "%s(): unable to find %s interface in %s service", __func__, ifce_name, dbus_service->name );
        return -2;
    }

    dbus_service_method_t *method = map_find( interface, (void *)name );
    if( method == NULL )
    {
        log_error( "%s(): unable to find %s method on %s interface", __func__, name, ifce_name );
        return -2;
    }

    DBusMessage *error = dbus_message_new_error( method->current_method.message, error_name, error_message );
    if( error == NULL )
    {
        log_error( "%s(): dbus_message_new_error failed", __func__ );
        return -3;
    }

    ret = dbus_connection_send( connection, error, NULL );
    if( ret == FALSE )
    {
        log_error( "%s(): dbus_connection_send failed", __func__ );
        dbus_message_unref( error );
        return -4;
    }

    dbus_message_unref(error);

    method->current_method.error_sent = TRUE;

    return 0;
}


static DBusHandlerResult dbus_service_filter( DBusConnection *connection, DBusMessage *message, void *user_data )
{
    log_debug( "%s(): enter", __func__ );
    if( dbus_message_is_signal( message, DBUS_INTERFACE_LOCAL, "Disconnected" ) )
    {
        kill( getpid(), SIGINT );
        return DBUS_HANDLER_RESULT_HANDLED;
    }
    else
    {
        return DBUS_HANDLER_RESULT_NOT_YET_HANDLED;
    }
}

static dbus_bool_t dbus_service_add_watch( DBusWatch * watch, void * data )
{
    log_debug( "%s(): enter", __func__ );
    int ret = io_add_watch_data( dbus_watch_get_unix_fd(watch), &dbus_service_io_config, (void *)watch );
    if( ret )
    {
        log_error( "%s(): io_add_watch_data failed with: %d", __func__, ret );
        return FALSE;
    }
    
    return TRUE;
}

static void dbus_service_remove_watch( DBusWatch * watch, void * data )
{
    log_debug( "%s(): enter", __func__ );
    io_del_watch_data( (void *)watch );
}

static void dbus_service_toggle_watch( DBusWatch * watch, void * data )
{
    log_debug( "%s(): enter", __func__ );
    dbus_service_remove_watch( watch, data );
    
    if( dbus_watch_get_enabled( watch ) == TRUE )
    {
        dbus_service_add_watch( watch, data );
    }
}

static void dbus_service_handle_dispatch_status( DBusConnection *connection, DBusDispatchStatus status, void *data )
{
    if( status == DBUS_DISPATCH_DATA_REMAINS )
    {
        log_debug( "%s(): DBUS_DISPATCH_DATA_REMAINS", __func__ );
        while( dbus_connection_get_dispatch_status(connection) == DBUS_DISPATCH_DATA_REMAINS )
        {
            dbus_connection_dispatch(connection);
        }
    }
    else if( status == DBUS_DISPATCH_COMPLETE )
    {
        log_debug( "%s(): DBUS_DISPATCH_COMPLETE", __func__ );
    }
}

static void dbus_service_unregister_handler( DBusConnection  *connection, void *data )
{
    log_debug( "%s(): enter", __func__ );
}

static DBusHandlerResult dbus_service_message_handler( DBusConnection *connection, DBusMessage *message, void *data )
{
    int ret;

    log_debug( "%s(): enter", __func__ );
    
    dbus_service_t *dbus_service = (dbus_service_t *)data;
    if( dbus_service == NULL )
    {
        log_error( "%s(): invalid arguments", __func__ );
        return DBUS_HANDLER_RESULT_HANDLED;
    }

    const char *member = dbus_message_get_member(message);
    if( member == NULL )
    {
        log_error( "%s(): dbus_message_get_member failed", __func__ );
        return DBUS_HANDLER_RESULT_NEED_MEMORY;
    }

    const char *ifce_name = dbus_message_get_interface(message);
    if( ifce_name == NULL )
    {
        log_error( "%s(): dbus_message_get_interface returned NULL", __func__ );
        ifce_name = dbus_service->name;
    }

    map_t *interface = dbus_service_get_interface( dbus_service, (char *)ifce_name );
    if( interface == NULL )
    {
        log_error( "%s(): unable to find %s interface in %s service", __func__, ifce_name, dbus_service->name );
        return DBUS_HANDLER_RESULT_HANDLED;
    }
    
    dbus_service_method_t *method = map_find( interface, (void *)member );
    if( method == NULL )
    {
        log_error( "%s(): unable to find %s method in %s service", __func__, member, dbus_service->name );
        return DBUS_HANDLER_RESULT_HANDLED;
    }

    log_debug( "%s(): handling %s method", __func__, member );

    method->current_method.message = message;
    method->current_method.reply_sent = FALSE;
    method->current_method.error_sent = FALSE;
    
    ret = method->method_handler( dbus_message_get_no_reply(message) == TRUE ? 0 : 1 , NULL );
    if( ret < 0 )
    {
        log_error( "%s(): (%s) method_handler failed with: %d", __func__, member, ret );
    }
    else
    {
        if( dbus_message_get_no_reply(message) == FALSE && method->current_method.reply_sent == FALSE && method->current_method.error_sent == FALSE )
        {
            log_error( "%s(): no reply sent, but requested by client", __func__ );
        }
    }

    method->current_method.message = NULL;
    method->current_method.reply_sent = FALSE;
    method->current_method.error_sent = FALSE;

    return DBUS_HANDLER_RESULT_HANDLED;
}

static int dbus_service_read_cb( int fd, void *data )
{
    log_debug( "%s(): enter", __func__ );
    
    DBusWatch *watch = (DBusWatch *)data;
    dbus_bool_t ret = dbus_watch_handle(watch, DBUS_WATCH_READABLE);
    if( ret == FALSE )
    {
        log_error( "%s(): dbus_watch_handle failed", __func__ );
        return -1;
    }

    dbus_service_handle_dispatch_status( connection, DBUS_DISPATCH_DATA_REMAINS, NULL );

    return 0;
}

static int dbus_service_timeout_cb( int fd, void *data )
{
    log_debug( "%s(): enter", __func__ );
    
    return 0;
}

static int name_compare( const void *name_1, const void *name_2 )
{
    return strcmp( *((const char **)name_1), (const char *)name_2 );
}

static map_t* dbus_service_get_interface( dbus_service_t *dbus_service, char *name )
{
    if( dbus_service == NULL || name == NULL )
    {
        log_error( "%s(): invalid arguments", __func__ );
        return NULL;
    }

    return (map_t *)map_find( &dbus_service->interfaces, name );
}

