/*
 * ----------------------------------------------------------------------------
 * "THE BEER-WARE LICENSE" (Revision 42):
 * <nowacki.jakub@gmail.com> wrote this file. As long as you retain this notice you
 * can do whatever you want with this stuff. If we meet some day, and you think
 * this stuff is worth it, you can buy me a beer in return Jakub Nowacki
 * ----------------------------------------------------------------------------
 */

#include <stdio.h>
#include <string.h>

#include <inttypes.h>

#include <dbus/dbus.h>

#include "dbus-common-debug.h"

char * dbus_common_debug_msg( const char * type )
{
    switch( type[0] )
    { 
        case DBUS_TYPE_BYTE:
            return "BYTE";
            break;
        case DBUS_TYPE_BOOLEAN:
            return "BOOLEAN";
            break;
        case DBUS_TYPE_INT16:
            return "INT16";
            break;
        case DBUS_TYPE_UINT16:
            return "UINT16";
            break;
        case DBUS_TYPE_INT32:
            return "INT32";
            break;
        case DBUS_TYPE_UINT32:
            return "UINT32";
            break;
        case DBUS_TYPE_INT64:
            return "INT64";
            break;
        case DBUS_TYPE_UINT64:
            return "UINT64";
            break;
        case DBUS_TYPE_DOUBLE:
            return "DOUBLE";
            break;
        case DBUS_TYPE_UNIX_FD:
            return "UNIX FD";
            break;
        case DBUS_TYPE_STRING:
            return "STRING";
            break;
        case DBUS_TYPE_ARRAY:
            return "ARRAY";
            break;
        case DBUS_TYPE_VARIANT:
            return "VARIANT";
            break;
        case DBUS_STRUCT_BEGIN_CHAR:
            return "STRUCT";
            break;
        case DBUS_DICT_ENTRY_BEGIN_CHAR:
            return "DICT ENTRY";
            break;
        default:
            return "";
    }
} 

char * dbus_common_debug_basic( const char * type, void * data )
{
    static char dbg_msg[sizeof(uint64_t) + 1];
    switch( type[0] )
    { 
        case DBUS_TYPE_DOUBLE:
            snprintf( dbg_msg, sizeof(dbg_msg), "%f", *((double *)data) );
            return dbg_msg;
            break;
        case DBUS_TYPE_BOOLEAN:
            if( *((int *)data) == 0 )
            {
                return "FALSE";
            }
            else
            { 
                return "TRUE";
            }
            break;
        case DBUS_TYPE_BYTE:
            snprintf( dbg_msg, sizeof(dbg_msg), "%u", *((uint8_t *)data) );
            return dbg_msg;
        case DBUS_TYPE_UINT16:
            snprintf( dbg_msg, sizeof(dbg_msg), "%u", *((uint16_t *)data) );
            return dbg_msg;
        case DBUS_TYPE_UINT32:
            snprintf( dbg_msg, sizeof(dbg_msg), "%u", *((uint32_t *)data) );
            return dbg_msg;
        case DBUS_TYPE_UINT64:
            snprintf( dbg_msg, sizeof(dbg_msg), "%lu", *((uint64_t *)data) );
            return dbg_msg;
        case DBUS_TYPE_UNIX_FD:
            snprintf( dbg_msg, sizeof(dbg_msg), "%u", *((uint32_t *)data) );
            return dbg_msg;
        case DBUS_TYPE_INT16:
            snprintf( dbg_msg, sizeof(dbg_msg), "%i", *((int16_t *)data) );
            return dbg_msg;
        case DBUS_TYPE_INT32:
            snprintf( dbg_msg, sizeof(dbg_msg), "%i", *((int32_t *)data) );
            return dbg_msg;
        case DBUS_TYPE_INT64:
            snprintf( dbg_msg, sizeof(dbg_msg), "%li", *((int64_t *)data) );
            return dbg_msg;
        case DBUS_TYPE_STRING:
            return *((char **)data);
            break;
        default:
            return "";
    }
}

char * dbus_common_get_indent( dbus_common_msg_level_t level )
{
    static size_t curr_level = 0;
    char single_indent[] = {"    "};
    static char indent[80];

    indent[0] = '\0';

    if( level == MSG_LEVEL_PRE_UP )
    {
        curr_level++;
    }
    else if( level == MSG_LEVEL_PRE_DOWN )
    {
        if( curr_level != 0 )
        {
            curr_level--;
        }
    }
    else if( level == MSG_LEVEL_PRE_RESET )
    {
        curr_level = 0;
    }
    
    for( size_t i = 0; i < curr_level; ++i )
    {
        strncpy( &indent[i*strlen(single_indent)], single_indent, strlen(single_indent) + 1 );
    }

    if( level == MSG_LEVEL_POST_UP )
    {
        curr_level++;
    }
    else if( level == MSG_LEVEL_POST_DOWN )
    {
        if( curr_level != 0 )
        {
            curr_level--;
        }
    }
    else if( level == MSG_LEVEL_POST_RESET )
    {
        curr_level = 0;
    }

    return indent;
}

