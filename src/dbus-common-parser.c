/*
 * ----------------------------------------------------------------------------
 * "THE BEER-WARE LICENSE" (Revision 42):
 * <nowacki.jakub@gmail.com> wrote this file. As long as you retain this notice you
 * can do whatever you want with this stuff. If we meet some day, and you think
 * this stuff is worth it, you can buy me a beer in return Jakub Nowacki
 * ----------------------------------------------------------------------------
 */

#include <dbus/dbus.h>

#include <ctools/log.h>

#include "dbus-common.h"
#include "dbus-common-debug.h"
#include "dbus-common-helpers.h"

#include <CVector.h>

static int dbus_common_parse_basic( DBusMessageIter *iter, void *data );
static int dbus_common_parse_variant( DBusMessageIter *iter, dbus_type_variant *variant );
static int dbus_common_parse_struct( DBusMessageIter *iter, const char *format, void *data );
static int dbus_common_parse_array( DBusMessageIter *iter, const char *format, CVectorHandle *data );
static int dbus_common_parse_dict_entry( DBusMessageIter *iter, const char *format, void *data );

int dbus_common_parse_message_args( DBusMessageIter *iter, const char * format, va_list va )
{
    int ret = 0;

    size_t idx = 0;
    char type[strlen(format)];
    char *signature = NULL;

    log_info( "%sstarting message parser, signature: %s", dbus_common_get_indent( MSG_LEVEL_POST_UP ), format );

    while( format[idx] != '\0' )
    {
        idx += dbus_common_decode_complete_type( &format[idx], type );

        log_info( "%snow parsing: %s", dbus_common_get_indent( MSG_LEVEL_POST_UP ), dbus_common_debug_msg(type) );
    
        signature = dbus_message_iter_get_signature( iter );
        if( signature == NULL )
        {
            log_error( "%s(): dbus_message_iter_get_signature failed - not enough memory", __func__ );
            return -1;
        }

        if( strcmp( signature, type ) != 0 )
        {
            log_error( "%s(): message signatrue(%s) differs from format(%s) passed to function", __func__, signature, format );
            return -1;
        }
        
        dbus_free( signature );
        
        switch( (int)type[0] )
        {
            case DBUS_STRUCT_BEGIN_CHAR:
                ret = dbus_common_parse_struct( iter, type, va_arg( va, void *) );
                break;
            case DBUS_TYPE_VARIANT:
                ret = dbus_common_parse_variant( iter, va_arg( va, void *) );
                break;
            case DBUS_TYPE_ARRAY:
                ret = dbus_common_parse_array( iter, type, va_arg( va, void *) );
                break;
            case '\0':
                break;
            default:
                {
                    /*TODO - add error handlig*/
                    void * data = va_arg(va, void *);
                    ret = dbus_common_parse_basic( iter, data );
                    log_info( "%svalue: %s", dbus_common_get_indent( MSG_LEVEL_STAY ), dbus_common_debug_basic( type, data ) );
                }
                break;
        }
        
        log_info( "%s%s - parsed", dbus_common_get_indent( MSG_LEVEL_PRE_DOWN ), dbus_common_debug_msg(type) );
        
        ret = dbus_message_iter_next( iter );
        if( ret == FALSE )
        {
            break;
        }
    }

    log_info( "%smessage(%s) parsed", dbus_common_get_indent( MSG_LEVEL_PRE_DOWN ), format );

    return 0;
}

static int dbus_common_parse_basic( DBusMessageIter *iter, void *data )
{
    dbus_message_iter_get_basic( iter, data );
    
    return 0;
}

static int dbus_common_parse_variant( DBusMessageIter *iter, dbus_type_variant *variant )
{
    DBusMessageIter sub;
    
    dbus_message_iter_recurse( iter, &sub );

    char *tmp_signature = dbus_message_iter_get_signature( &sub );
    if( tmp_signature == NULL )
    {
        log_error( "%s(): dbus_message_iter_get_signature failed - not enough memory", __func__ );
        return -1;
    }

    strcpy( variant->signature, tmp_signature );

    dbus_free( tmp_signature );

    log_info( "%sfound variant element, type: %s", dbus_common_get_indent( MSG_LEVEL_POST_UP ), dbus_common_debug_msg( variant->signature ) );
    
    switch( variant->signature[0] )
    {
        case DBUS_STRUCT_BEGIN_CHAR:
            {
                size_t element_size = dbus_common_calculate_complete_type_size( variant->signature );
                variant->STRUCT = malloc( element_size );
                if( variant->STRUCT == NULL )
                {
                    /*TODO - add error handlig*/
                }
                /*TODO - add error handlig*/
                dbus_common_parse_struct( &sub, variant->signature, variant->STRUCT );
                break;
            }
            break;
        case DBUS_TYPE_VARIANT:
            variant->VARIANT = malloc( sizeof(dbus_type_variant) );
            if( variant->VARIANT )
            {
                /*TODO - add error handlig*/
            }
            /*TODO - add error handlig*/
            dbus_common_parse_variant( &sub, variant->VARIANT );
            break;
        case DBUS_TYPE_ARRAY:
            dbus_common_parse_array( &sub, variant->signature, &variant->ARRAY );
            break;
        case DBUS_TYPE_STRING:
            dbus_common_parse_basic( &sub, &variant->STRING );
            log_info( "%svalue: %s", dbus_common_get_indent( MSG_LEVEL_STAY ), variant->STRING );
            break;
        case '\0':
            break;
        default:
            /*TODO - add error handlig*/
            dbus_common_parse_basic( &sub, &variant->DATA );
            log_info( "%svalue: %s", dbus_common_get_indent( MSG_LEVEL_STAY ), dbus_common_debug_basic( variant->signature, &variant->DATA ) );
            break;
    }
    
    dbus_common_get_indent( MSG_LEVEL_PRE_DOWN );

    return 0;
}

static int dbus_common_parse_struct( DBusMessageIter *iter, const char *format, void *data )
{
    size_t idx = 1;

    DBusMessageIter sub;
    dbus_message_iter_recurse( iter, &sub );
    
    char type[strlen(format)];

    while( idx < strlen( format ) )
    {
        idx += dbus_common_decode_complete_type( &format[idx], type );
        switch( type[0] )
        {
            case DBUS_TYPE_BYTE:
                log_info( "%sfound struct element, type: %s", dbus_common_get_indent( MSG_LEVEL_POST_UP ), dbus_common_debug_msg(type) );
                /*TODO - add error handlig*/
                dbus_common_parse_basic( &sub, data );
                log_info( "%svalue: %s", dbus_common_get_indent( MSG_LEVEL_POST_DOWN ), dbus_common_debug_basic( type, data ) );
                data += sizeof(dbus_type_byte); 
                dbus_message_iter_next( &sub );
                break;
            case DBUS_TYPE_BOOLEAN:
                log_info( "%sfound struct element, type: %s", dbus_common_get_indent( MSG_LEVEL_POST_UP ), dbus_common_debug_msg(type) );
                /*TODO - add error handlig*/
                dbus_common_parse_basic( &sub, data );
                log_info( "%svalue: %s", dbus_common_get_indent( MSG_LEVEL_POST_DOWN ), dbus_common_debug_basic( type, data ) );
                data += sizeof(dbus_type_uint32);
                dbus_message_iter_next( &sub );
                break;
            case DBUS_TYPE_INT16:
                log_info( "%sfound struct element, type: %s", dbus_common_get_indent( MSG_LEVEL_POST_UP ), dbus_common_debug_msg(type) );
                /*TODO - add error handlig*/
                dbus_common_parse_basic( &sub, data );
                log_info( "%svalue: %s", dbus_common_get_indent( MSG_LEVEL_POST_DOWN ), dbus_common_debug_basic( type, data ) );
                data += sizeof(dbus_type_int16);
                dbus_message_iter_next( &sub );
                break;
            case DBUS_TYPE_UINT16:
                log_info( "%sfound struct element, type: %s", dbus_common_get_indent( MSG_LEVEL_POST_UP ), dbus_common_debug_msg(type) );
                /*TODO - add error handlig*/
                dbus_common_parse_basic( &sub, data );
                log_info( "%svalue: %s", dbus_common_get_indent( MSG_LEVEL_POST_DOWN ), dbus_common_debug_basic( type, data ) );
                data += sizeof(dbus_type_uint16);
                dbus_message_iter_next( &sub );
                break;
            case DBUS_TYPE_INT32:
                log_info( "%sfound struct element, type: %s", dbus_common_get_indent( MSG_LEVEL_POST_UP ), dbus_common_debug_msg(type) );
                /*TODO - add error handlig*/
                dbus_common_parse_basic( &sub, data );
                log_info( "%svalue: %s", dbus_common_get_indent( MSG_LEVEL_POST_DOWN ), dbus_common_debug_basic( type, data ) );
                data += sizeof(dbus_type_int32);
                dbus_message_iter_next( &sub );
                break;
            case DBUS_TYPE_UINT32:
                log_info( "%sfound struct element, type: %s", dbus_common_get_indent( MSG_LEVEL_POST_UP ), dbus_common_debug_msg(type) );
                /*TODO - add error handlig*/
                dbus_common_parse_basic( &sub, data );
                log_info( "%svalue: %s", dbus_common_get_indent( MSG_LEVEL_POST_DOWN ), dbus_common_debug_basic( type, data ) );
                data += sizeof(dbus_type_uint32);
                dbus_message_iter_next( &sub );
                break;
            case DBUS_TYPE_INT64:
                log_info( "%sfound struct element, type: %s", dbus_common_get_indent( MSG_LEVEL_POST_UP ), dbus_common_debug_msg(type) );
                /*TODO - add error handlig*/
                dbus_common_parse_basic( &sub, data );
                log_info( "%svalue: %s", dbus_common_get_indent( MSG_LEVEL_POST_DOWN ), dbus_common_debug_basic( type, data ) );
                data += sizeof(dbus_type_int64);
                dbus_message_iter_next( &sub );
                break;
            case DBUS_TYPE_UINT64:
                log_info( "%sfound struct element, type: %s", dbus_common_get_indent( MSG_LEVEL_POST_UP ), dbus_common_debug_msg(type) );
                /*TODO - add error handlig*/
                dbus_common_parse_basic( &sub, data );
                log_info( "%svalue: %s", dbus_common_get_indent( MSG_LEVEL_POST_DOWN ), dbus_common_debug_basic( type, data ) );
                data += sizeof(dbus_type_uint64);
                dbus_message_iter_next( &sub );
                break;
            case DBUS_TYPE_DOUBLE:
                log_info( "%sfound struct element, type: %s", dbus_common_get_indent( MSG_LEVEL_POST_UP ), dbus_common_debug_msg(type) );
                /*TODO - add error handlig*/
                dbus_common_parse_basic( &sub, data );
                log_info( "%svalue: %s", dbus_common_get_indent( MSG_LEVEL_POST_DOWN ), dbus_common_debug_basic( type, data ) );
                data += sizeof(dbus_type_double);
                dbus_message_iter_next( &sub );
                break;
            case DBUS_TYPE_STRING:
                log_info( "%sfound struct element, type: %s", dbus_common_get_indent( MSG_LEVEL_POST_UP ), dbus_common_debug_msg(type) );
                /*TODO - add error handlig*/
                dbus_common_parse_basic( &sub, data );
                log_info( "%svalue: %s", dbus_common_get_indent( MSG_LEVEL_POST_DOWN ), dbus_common_debug_basic( type, data ) );
                data += sizeof(dbus_type_string);
                dbus_message_iter_next( &sub );
                break;
            case DBUS_TYPE_UNIX_FD:
                log_info( "%sfound struct element, type: %s", dbus_common_get_indent( MSG_LEVEL_POST_UP ), dbus_common_debug_msg(type) );
                /*TODO - add error handlig*/
                dbus_common_parse_basic( &sub, data );
                log_info( "%svalue: %s", dbus_common_get_indent( MSG_LEVEL_POST_DOWN ), dbus_common_debug_basic( type, data ) );
                data += sizeof(dbus_type_unix_fd);
                dbus_message_iter_next( &sub );
                break;
            case DBUS_TYPE_ARRAY:
                log_info( "%sfound struct element, type: %s", dbus_common_get_indent( MSG_LEVEL_POST_UP ), dbus_common_debug_msg(type) );
                /*TODO - add error handlig*/
                dbus_common_parse_array( &sub, &type[0], data );
                dbus_common_get_indent( MSG_LEVEL_POST_DOWN );
                data += sizeof(dbus_type_array);
                dbus_message_iter_next( &sub );
                break;
            case DBUS_STRUCT_BEGIN_CHAR:
                log_info( "%sfound struct element, type: %s", dbus_common_get_indent( MSG_LEVEL_POST_UP ), dbus_common_debug_msg(type) );
                /*TODO - add error handlig*/
                dbus_common_parse_struct( &sub, type, data );
                dbus_common_get_indent( MSG_LEVEL_POST_DOWN );
                data += dbus_common_calculate_complete_type_size( type );
                dbus_message_iter_next( &sub );
                break;
            case DBUS_TYPE_VARIANT:
                log_info( "%sfound struct element, type: %s", dbus_common_get_indent( MSG_LEVEL_POST_UP ), dbus_common_debug_msg(type) );
                /*TODO - add error handlig*/
                dbus_common_parse_variant( &sub, data );
                dbus_common_get_indent( MSG_LEVEL_POST_DOWN );
                data += sizeof(dbus_type_variant);
                dbus_message_iter_next( &sub );
            case DBUS_DICT_ENTRY_END_CHAR:
            case DBUS_STRUCT_END_CHAR:
                break;
            default:
                log_info( "%s(): case hit default on type: %s - that should not happen(either bug in signature or DBUS has changed)", __func__, type );
                break;
        }
    }
    
    return 0;
}

static int dbus_common_parse_array( DBusMessageIter *iter, const char *format, CVectorHandle *data )
{
    int ret = 0;
    DBusMessageIter sub;

    size_t element_size = dbus_common_calculate_complete_type_size( &format[1] );
    ret = CVectorCreate( data, element_size, 2 );
    if( ret != 0 )
    {
        log_error( "%s(): CVectorCreate failed with: %d", __func__, ret );
        return -1;
    }
    
    dbus_message_iter_recurse( iter, &sub );
    if( dbus_message_iter_get_arg_type( &sub ) == DBUS_TYPE_INVALID )
    {
        log_info( "array has no elements" );
        return 0;
    }
    
    log_info( "%sparsing array of %s", dbus_common_get_indent( MSG_LEVEL_POST_UP ), dbus_common_debug_msg(&format[1]) );

    do
    {
        switch( format[1] )
        {
            case DBUS_TYPE_BYTE:
                {
                    dbus_type_byte tmp;
                    /*TODO - add error handlig*/
                    dbus_common_parse_basic( &sub, &tmp );
                    log_info( "%svalue: %s", dbus_common_get_indent( MSG_LEVEL_STAY ), dbus_common_debug_basic( &format[1], &tmp ) );
                    CVectorAddElement( *data, &tmp );
                    break;
                }
            case DBUS_TYPE_BOOLEAN:
                {
                    dbus_type_boolean tmp;
                    /*TODO - add error handlig*/
                    dbus_common_parse_basic( &sub, &tmp );
                    log_info( "%svalue: %s", dbus_common_get_indent( MSG_LEVEL_STAY ), dbus_common_debug_basic( &format[1], &tmp ) );
                    CVectorAddElement( *data, &tmp );
                    break;
                }
            case DBUS_TYPE_INT16:
                {
                    dbus_type_int16 tmp;
                    /*TODO - add error handlig*/
                    dbus_common_parse_basic( &sub, &tmp );
                    log_info( "%svalue: %s", dbus_common_get_indent( MSG_LEVEL_STAY ), dbus_common_debug_basic( &format[1], &tmp ) );
                    CVectorAddElement( *data, &tmp );
                    break;
                }
            case DBUS_TYPE_UINT16:
                {
                    dbus_type_uint16 tmp;
                    /*TODO - add error handlig*/
                    dbus_common_parse_basic( &sub, &tmp );
                    log_info( "%svalue: %s", dbus_common_get_indent( MSG_LEVEL_STAY ), dbus_common_debug_basic( &format[1], &tmp ) );
                    CVectorAddElement( *data, &tmp );
                    break;
                }
            case DBUS_TYPE_INT32:
                {
                    dbus_type_int32 tmp;
                    /*TODO - add error handlig*/
                    dbus_common_parse_basic( &sub, &tmp );
                    log_info( "%svalue: %s", dbus_common_get_indent( MSG_LEVEL_STAY ), dbus_common_debug_basic( &format[1], &tmp ) );
                    CVectorAddElement( *data, &tmp );
                    break;
                }
            case DBUS_TYPE_UINT32:
                {
                    dbus_type_uint32 tmp;
                    /*TODO - add error handlig*/
                    dbus_common_parse_basic( &sub, &tmp );
                    log_info( "%svalue: %s", dbus_common_get_indent( MSG_LEVEL_STAY ), dbus_common_debug_basic( &format[1], &tmp ) );
                    CVectorAddElement( *data, &tmp );
                    break;
                }
            case DBUS_TYPE_INT64:
                {
                    dbus_type_int64 tmp;
                    /*TODO - add error handlig*/
                    dbus_common_parse_basic( &sub, &tmp );
                    log_info( "%svalue: %s", dbus_common_get_indent( MSG_LEVEL_STAY ), dbus_common_debug_basic( &format[1], &tmp ) );
                    CVectorAddElement( *data, &tmp );
                    break;
                }
            case DBUS_TYPE_UINT64:
                {
                    dbus_type_uint64 tmp;
                    /*TODO - add error handlig*/
                    dbus_common_parse_basic( &sub, &tmp );
                    log_info( "%svalue: %s", dbus_common_get_indent( MSG_LEVEL_STAY ), dbus_common_debug_basic( &format[1], &tmp ) );
                    CVectorAddElement( *data, &tmp );
                    break;
                }
            case DBUS_TYPE_DOUBLE:
                {
                    dbus_type_double tmp;
                    /*TODO - add error handlig*/
                    dbus_common_parse_basic( &sub, &tmp );
                    log_info( "%svalue: %s", dbus_common_get_indent( MSG_LEVEL_STAY ), dbus_common_debug_basic( &format[1], &tmp ) );
                    CVectorAddElement( *data, &tmp );
                    break;
                }
            case DBUS_TYPE_STRING:
                {
                    dbus_type_string tmp;
                    /*TODO - add error handlig*/
                    dbus_common_parse_basic( &sub, &tmp );
                    log_info( "%svalue: %s", dbus_common_get_indent( MSG_LEVEL_STAY ), dbus_common_debug_basic( &format[1], &tmp ) );
                    CVectorAddElement( *data, &tmp );
                    break;
                }
            case DBUS_TYPE_UNIX_FD:
                {
                    uint32_t tmp;
                    /*TODO - add error handlig*/
                    dbus_common_parse_basic( &sub, &tmp );
                    log_info( "%svalue: %s", dbus_common_get_indent( MSG_LEVEL_STAY ), dbus_common_debug_basic( &format[1], &tmp ) );
                    CVectorAddElement( *data, &tmp );
                    break;
                }
            case DBUS_STRUCT_BEGIN_CHAR:
                {
                    dbus_type_struct tmp = malloc( (*data)->elementsize );
                    if( tmp == NULL )
                    {
                        /*TODO - add error handlig*/
                    }
                    /*TODO - add error handlig*/
                    dbus_common_parse_struct( &sub, &format[1], tmp );
                    CVectorAddElement( *data, tmp );
                    free( tmp );
                    break;
                }
            case DBUS_DICT_ENTRY_BEGIN_CHAR:
                {
                    dbus_type_struct tmp = malloc( (*data)->elementsize );
                    if( tmp == NULL )
                    {
                        /*TODO - add error handlig*/
                    }
                    /*TODO - add error handlig*/
                    dbus_common_parse_dict_entry( &sub, &format[1], tmp );
                    CVectorAddElement( *data, tmp );
                    free( tmp );
                    break;
                }
            case DBUS_TYPE_VARIANT:
                {
                    dbus_type_variant tmp;
                    /*TODO - add error handlig*/
                    dbus_common_parse_variant( &sub, &tmp );
                    CVectorAddElement( *data, &tmp );
                    break;
                }
            case DBUS_TYPE_ARRAY:
                {
                    dbus_type_array tmp;
                    /*TODO - add error handlig*/
                    dbus_common_parse_array( &sub, &format[1], &tmp );
                    CVectorAddElement( *data, &tmp );
                    break;
                }
            default:
                break;
        }
    } while( dbus_message_iter_next( &sub ) );

    dbus_common_get_indent( MSG_LEVEL_PRE_DOWN );

    return 0;
}

static int dbus_common_parse_dict_entry( DBusMessageIter *iter, const char *format, void *data )
{
    size_t idx = 1;

    DBusMessageIter sub;
    dbus_message_iter_recurse( iter, &sub );
    
    char type[strlen(format)];
    
    while( idx < strlen( format ) )
    {
        idx += dbus_common_decode_complete_type( &format[idx], type );
        
        switch( type[0] )
        {
            case DBUS_TYPE_BYTE:
                log_info( "%sfound dict entry element, type: %s", dbus_common_get_indent( MSG_LEVEL_POST_UP ), dbus_common_debug_msg(type) );
                /*TODO - add error handlig*/
                dbus_common_parse_basic( &sub, data );
                log_info( "%svalue: %s", dbus_common_get_indent( MSG_LEVEL_POST_DOWN ), dbus_common_debug_basic( type, data ) );
                data += sizeof(dbus_type_byte); 
                dbus_message_iter_next( &sub );
                break;
            case DBUS_TYPE_BOOLEAN:
                log_info( "%sfound dict entry element, type: %s", dbus_common_get_indent( MSG_LEVEL_POST_UP ), dbus_common_debug_msg(type) );
                /*TODO - add error handlig*/
                dbus_common_parse_basic( &sub, data );
                log_info( "%svalue: %s", dbus_common_get_indent( MSG_LEVEL_POST_DOWN ), dbus_common_debug_basic( type, data ) );
                data += sizeof(dbus_type_uint32);
                dbus_message_iter_next( &sub );
                break;
            case DBUS_TYPE_INT16:
                log_info( "%sfound dict entry element, type: %s", dbus_common_get_indent( MSG_LEVEL_POST_UP ), dbus_common_debug_msg(type) );
                /*TODO - add error handlig*/
                dbus_common_parse_basic( &sub, data );
                log_info( "%svalue: %s", dbus_common_get_indent( MSG_LEVEL_POST_DOWN ), dbus_common_debug_basic( type, data ) );
                data += sizeof(dbus_type_int16);
                dbus_message_iter_next( &sub );
                break;
            case DBUS_TYPE_UINT16:
                log_info( "%sfound dict entry element, type: %s", dbus_common_get_indent( MSG_LEVEL_POST_UP ), dbus_common_debug_msg(type) );
                /*TODO - add error handlig*/
                dbus_common_parse_basic( &sub, data );
                log_info( "%svalue: %s", dbus_common_get_indent( MSG_LEVEL_POST_DOWN ), dbus_common_debug_basic( type, data ) );
                data += sizeof(dbus_type_uint16);
                dbus_message_iter_next( &sub );
                break;
            case DBUS_TYPE_INT32:
                log_info( "%sfound dict entry element, type: %s", dbus_common_get_indent( MSG_LEVEL_POST_UP ), dbus_common_debug_msg(type) );
                /*TODO - add error handlig*/
                dbus_common_parse_basic( &sub, data );
                log_info( "%svalue: %s", dbus_common_get_indent( MSG_LEVEL_POST_DOWN ), dbus_common_debug_basic( type, data ) );
                data += sizeof(dbus_type_int32);
                dbus_message_iter_next( &sub );
                break;
            case DBUS_TYPE_UINT32:
                log_info( "%sfound dict entry element, type: %s", dbus_common_get_indent( MSG_LEVEL_POST_UP ), dbus_common_debug_msg(type) );
                /*TODO - add error handlig*/
                dbus_common_parse_basic( &sub, data );
                log_info( "%svalue: %s", dbus_common_get_indent( MSG_LEVEL_POST_DOWN ), dbus_common_debug_basic( type, data ) );
                data += sizeof(dbus_type_uint32);
                dbus_message_iter_next( &sub );
                break;
            case DBUS_TYPE_INT64:
                log_info( "%sfound dict entry element, type: %s", dbus_common_get_indent( MSG_LEVEL_POST_UP ), dbus_common_debug_msg(type) );
                /*TODO - add error handlig*/
                dbus_common_parse_basic( &sub, data );
                data += sizeof(dbus_type_int64);
                dbus_message_iter_next( &sub );
                break;
            case DBUS_TYPE_UINT64:
                log_info( "%sfound dict entry element, type: %s", dbus_common_get_indent( MSG_LEVEL_POST_UP ), dbus_common_debug_msg(type) );
                /*TODO - add error handlig*/
                dbus_common_parse_basic( &sub, data );
                log_info( "%svalue: %s", dbus_common_get_indent( MSG_LEVEL_POST_DOWN ), dbus_common_debug_basic( type, data ) );
                data += sizeof(dbus_type_uint64);
                dbus_message_iter_next( &sub );
                break;
            case DBUS_TYPE_DOUBLE:
                log_info( "%sfound dict entry element, type: %s", dbus_common_get_indent( MSG_LEVEL_POST_UP ), dbus_common_debug_msg(type) );
                /*TODO - add error handlig*/
                dbus_common_parse_basic( &sub, data );
                log_info( "%svalue: %s", dbus_common_get_indent( MSG_LEVEL_POST_DOWN ), dbus_common_debug_basic( type, data ) );
                data += sizeof(dbus_type_double);
                dbus_message_iter_next( &sub );
                break;
            case DBUS_TYPE_STRING:
                log_info( "%sfound dict entry element, type: %s", dbus_common_get_indent( MSG_LEVEL_POST_UP ), dbus_common_debug_msg(type) );
                /*TODO - add error handlig*/
                dbus_common_parse_basic( &sub, data );
                log_info( "%svalue: %s", dbus_common_get_indent( MSG_LEVEL_POST_DOWN ), dbus_common_debug_basic( type, data ) );
                data += sizeof(dbus_type_string);
                dbus_message_iter_next( &sub );
                break;
            case DBUS_TYPE_UNIX_FD:
                log_info( "%sfound dict entry element, type: %s", dbus_common_get_indent( MSG_LEVEL_POST_UP ), dbus_common_debug_msg(type) );
                /*TODO - add error handlig*/
                dbus_common_parse_basic( &sub, data );
                log_info( "%svalue: %s", dbus_common_get_indent( MSG_LEVEL_POST_DOWN ), dbus_common_debug_basic( type, data ) );
                data += sizeof(dbus_type_unix_fd);
                dbus_message_iter_next( &sub );
                break;
            case DBUS_TYPE_ARRAY:
                log_info( "%sfound dict entry element, type: %s", dbus_common_get_indent( MSG_LEVEL_POST_UP ), dbus_common_debug_msg(type) );
                /*TODO - add error handlig*/
                dbus_common_parse_array( &sub, &type[0], data );
                dbus_common_get_indent( MSG_LEVEL_POST_DOWN );
                data += sizeof(dbus_type_array);
                dbus_message_iter_next( &sub );
                break;
            case DBUS_STRUCT_BEGIN_CHAR:
                log_info( "%sfound dict entry element, type: %s", dbus_common_get_indent( MSG_LEVEL_POST_UP ), dbus_common_debug_msg(type) );
                /*TODO - add error handlig*/
                dbus_common_parse_struct( &sub, type, data );
                dbus_common_get_indent( MSG_LEVEL_POST_DOWN );
                data += dbus_common_calculate_complete_type_size( type );
                dbus_message_iter_next( &sub );
                break;
            case DBUS_TYPE_VARIANT:
                log_info( "%sfound dict entry element, type: %s", dbus_common_get_indent( MSG_LEVEL_POST_UP ), dbus_common_debug_msg(type) );
                /*TODO - add error handlig*/
                dbus_common_parse_variant( &sub, data );
                dbus_common_get_indent( MSG_LEVEL_POST_DOWN );
                data += sizeof(dbus_type_variant);
                dbus_message_iter_next( &sub );
            case DBUS_DICT_ENTRY_END_CHAR:
            case DBUS_STRUCT_END_CHAR:
                break;
            default:
                log_info( "%s(): case hit default on type: %s - that should not happen(either bug in signature or DBUS has changed)", __func__, type );
                break;
        }
    }

    return 0;
}

