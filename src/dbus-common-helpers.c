/*
 * ----------------------------------------------------------------------------
 * "THE BEER-WARE LICENSE" (Revision 42):
 * <nowacki.jakub@gmail.com> wrote this file. As long as you retain this notice you
 * can do whatever you want with this stuff. If we meet some day, and you think
 * this stuff is worth it, you can buy me a beer in return Jakub Nowacki
 * ----------------------------------------------------------------------------
 */

#include <dbus/dbus.h>

#include "dbus-types.h"

int dbus_common_decode_complete_type( const char *in, char *out )
{
    size_t in_idx = 0;
    size_t out_idx = 0;

    size_t depth = 0;

    while( in[in_idx] != '\0' )
    {
        switch( in[in_idx] )
        {
            case DBUS_STRUCT_BEGIN_CHAR:
            case DBUS_DICT_ENTRY_BEGIN_CHAR:
                depth++;
                out[out_idx++] = in[in_idx++];
                break;
            case DBUS_STRUCT_END_CHAR:
            case DBUS_DICT_ENTRY_END_CHAR:
                depth--;
                out[out_idx++] = in[in_idx++];
                break;
            case DBUS_TYPE_ARRAY:
                {
                    out[out_idx++] = in[in_idx++];
                    size_t offset = dbus_common_decode_complete_type( &in[in_idx], &out[out_idx] );
                    out_idx += offset;
                    in_idx += offset;
                }
                break;
            default:
                out[out_idx++] = in[in_idx++];
                break;
        }

        if( depth == 0 )
            break;
    }

    out[out_idx] = '\0';

    return in_idx;
}

size_t dbus_common_calculate_complete_type_size( const char *signature )
{
    size_t size = 0;
    
    for( size_t idx = 0; idx < strlen(signature); ++idx )
    {
        switch( (int)signature[idx] )
        {
            case DBUS_TYPE_BYTE:
                size += sizeof(dbus_type_byte); 
                break;
            case DBUS_TYPE_BOOLEAN:
                size += sizeof(dbus_type_boolean);
                break;
            case DBUS_TYPE_INT16:
                size += sizeof(dbus_type_int16);
                break;
            case DBUS_TYPE_UINT16:
                size += sizeof(dbus_type_uint16);
                break;
            case DBUS_TYPE_INT32:
                size += sizeof(dbus_type_int32);
                break;
            case DBUS_TYPE_UINT32:
                size += sizeof(dbus_type_uint32);
                break;
            case DBUS_TYPE_INT64:
                size += sizeof(dbus_type_int64);
                break;
            case DBUS_TYPE_UINT64:
                size += sizeof(dbus_type_uint64);
                break;
            case DBUS_TYPE_DOUBLE:
                size += sizeof(dbus_type_double);
                break;
            case DBUS_TYPE_STRING:
                size += sizeof(dbus_type_string);
                break;
            case DBUS_TYPE_UNIX_FD:
                size += sizeof(dbus_type_unix_fd);
                break;
            case DBUS_TYPE_VARIANT:
                size += sizeof(dbus_type_variant);
                break;
            case DBUS_TYPE_ARRAY:
                size += sizeof(dbus_type_array);
                idx++;
                if( signature[idx] == DBUS_STRUCT_BEGIN_CHAR )
                {
                    idx += strchr( &signature[idx], DBUS_STRUCT_END_CHAR ) - &signature[idx];
                }
                else if( signature[idx] == DBUS_DICT_ENTRY_BEGIN_CHAR )
                {
                    idx += strchr( &signature[idx], DBUS_DICT_ENTRY_END_CHAR ) - &signature[idx];
                }
                break;
            default:
                break;
        }
    }

    return size;
}

