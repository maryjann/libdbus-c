#!/bin/bash

output_file="../inc/dbus-types.h"

if [ -f $output_file ]; then
    exit
fi

out=`$1 -E ../inc/dbus-types-in.h ${@:2}`
start_line=`echo "$out" | grep -m 1 -n "typedef uint8_t" | cut -d':' -f1`
end_line=`echo "$out" | grep -n "dbus_bus_type_t" | cut -d':' -f1`

echo "/*
 * ----------------------------------------------------------------------------
  "THE BEER-WARE LICENSE" (Revision 42):
 * <maryjann@gmail.com> wrote this file. As long as you retain this notice you
 * can do whatever you want with this stuff. If we meet some day, and you think
 * this stuff is worth it, you can buy me a beer in return Jakub Nowacki
 * ----------------------------------------------------------------------------
*/
" > $output_file

echo "#ifndef _DBUS_TYPES_H_" >> $output_file
echo "#define _DBUS_TYPES_H_" >> $output_file
echo >> $output_file
echo "#include <inttypes.h>" >> $output_file
echo "#include <CVector.h>" >> $output_file
echo >> $output_file
echo "#define DBUS_STRUCT struct __attribute__((__packed__))" >> $output_file
echo >> $output_file
echo "$out" | sed -n "$start_line,$end_line"p >> $output_file
echo >> $output_file
echo "#endif" >> $output_file
