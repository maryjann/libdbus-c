/*
 * ----------------------------------------------------------------------------
 * "THE BEER-WARE LICENSE" (Revision 42):
 * <nowacki.jakub@gmail.com> wrote this file. As long as you retain this notice you
 * can do whatever you want with this stuff. If we meet some day, and you think
 * this stuff is worth it, you can buy me a beer in return Jakub Nowacki
 * ----------------------------------------------------------------------------
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <signal.h>

#include <unistd.h>

#include <CVector.h>

#include <ctools/log.h>
#include <ctools/io.h>
#include "dbus-client.h"
#include "dbus-types.h"

DBUS_STRUCT struct_1
{
    dbus_type_uint32 uint32_1;
    dbus_type_string string_1;
};

DBUS_STRUCT struct_2
{
    dbus_type_string string_1;
    dbus_type_string string_2;
};

DBUS_STRUCT struct_3
{
    struct struct_1 struct_1;
    struct struct_2 struct_2;
};

DBUS_STRUCT dict_entry_1
{
    dbus_type_string key;
    dbus_type_uint32 value;
};

static int c1_reply_handler_2( void *data );
static int c1_reply_handler_3( void *data );
static int c1_reply_handler_4( void *data );
static int c1_reply_handler_5( void *data );
static int c1_reply_handler_6( void *data );
static int c1_reply_handler_7( void *data );
static int c1_reply_handler_8( void *data );
static int c1_reply_handler_9( void *data );
static int c1_reply_handler_10( void *data );
    
static dbus_client_t dbus_client_1;

static void sig_int_handler(int signo);

static int run = 1;

int main( int argc, char *argv[] )
{
    int ret = 0;

    signal(SIGINT, sig_int_handler);
    
    ret = log_init( "dbus-client-test", 1, 6 );
    if( ret < 0 )
    {
        log_error( "%s(): log_init failed with: %d", __func__, ret );
        return -1;
    }
    log_info( "%s(): log_init succeeded", __func__ );

    ret = io_init();
    if( ret < 0 )
    {
        log_error( "%s(): io_init failed with: %d", __func__, ret );
        ret = -2;
        goto main_log_init_cleanup;
    }
    log_info( "%s(): io_init succeeded", __func__ );
    
    dbus_client_init( DBUS_BUS_TYPE_SESSION );
    if( ret < 0 )
    {
        log_error( "%s(): dbus_client_init failed with: %d", __func__, ret );
        ret = -3;
        goto main_io_init_cleanup;
    }
    log_info( "%s(): dbus_client_init succeeded", __func__ );

    ret = dbus_client_register_client( &dbus_client_1, "/org/freedesktop/DBusService1", "org.freedesktop.DBusService1" );
    if( ret < 0 )
    {
        log_error( "%s(): dbus_client_register_client failed with: %d", __func__, ret );
        ret = -4;
        goto main_dbus_client_init_cleanup;
    }

    ret = dbus_client_register_reply( &dbus_client_1, "method2", c1_reply_handler_2 );
    if( ret < 0 )
    {
        log_error( "%s(): dbus_client_register_reply failed with: %d", __func__, ret );
        ret = -6;
        goto main_dbus_client_register_client_cleanup;
    }
    
    ret = dbus_client_register_reply( &dbus_client_1, "method3", c1_reply_handler_3 );
    if( ret < 0 )
    {
        log_error( "%s(): dbus_client_register_reply failed with: %d", __func__, ret );
        ret = -6;
        goto main_dbus_client_register_reply_2_cleanup; 
    }

    ret = dbus_client_register_reply( &dbus_client_1, "method4", c1_reply_handler_4 );
    if( ret < 0 )
    {
        log_error( "%s(): dbus_client_register_reply failed with: %d", __func__, ret );
        ret = -6;
        goto main_dbus_client_register_reply_3_cleanup; 
    }
    
    ret = dbus_client_register_reply( &dbus_client_1, "method5", c1_reply_handler_5 );
    if( ret < 0 )
    {
        log_error( "%s(): dbus_client_register_reply failed with: %d", __func__, ret );
        ret = -6;
        goto main_dbus_client_register_reply_4_cleanup; 
    }
    
    ret = dbus_client_register_reply( &dbus_client_1, "method6", c1_reply_handler_6 );
    if( ret < 0 )
    {
        log_error( "%s(): dbus_client_register_reply failed with: %d", __func__, ret );
        ret = -6;
        goto main_dbus_client_register_reply_5_cleanup; 
    }

    ret = dbus_client_register_reply( &dbus_client_1, "method7", c1_reply_handler_7 );
    if( ret < 0 )
    {
        log_error( "%s(): dbus_client_register_reply failed with: %d", __func__, ret );
        ret = -6;
        goto main_dbus_client_register_reply_6_cleanup; 
    }

    ret = dbus_client_register_reply( &dbus_client_1, "method8", c1_reply_handler_8 );
    if( ret < 0 )
    {
        log_error( "%s(): dbus_client_register_reply failed with: %d", __func__, ret );
        ret = -6;
        goto main_dbus_client_register_reply_7_cleanup; 
    }
    
    ret = dbus_client_register_reply( &dbus_client_1, "method9", c1_reply_handler_9 );
    if( ret < 0 )
    {
        log_error( "%s(): dbus_client_register_reply failed with: %d", __func__, ret );
        ret = -6;
        goto main_dbus_client_register_reply_8_cleanup; 
    }
    
    ret = dbus_client_register_reply( &dbus_client_1, "method10", c1_reply_handler_10 );
    if( ret < 0 )
    {
        log_error( "%s(): dbus_client_register_reply failed with: %d", __func__, ret );
        ret = -6;
        goto main_dbus_client_register_reply_9_cleanup; 
    }

    ret = 0;

    dbus_type_uint32 uint32_1 = 0xDEADBEEF;
    dbus_type_string string_1 = "STRING_1";
    dbus_type_string string_2 = "STRING_2";
    
    struct struct_1 struct_1 = { 0xDEADBEEF, string_1 };

    dbus_type_array vector_1;
    CVectorCreate( &vector_1, sizeof( char * ), 2 );
    CVectorAddElement( vector_1, &string_1 );
    CVectorAddElement( vector_1, &string_2 );

    dbus_type_variant variant_1 = { .signature = "s",
                                    .STRING = string_1 };
    
    struct struct_2 struct_2;
    struct_2.string_1 = string_1;
    struct_2.string_2 = string_2;
    dbus_type_array vector_2;
    CVectorCreate( &vector_2, sizeof( struct struct_2 ), 2 );
    CVectorAddElement( vector_2, &struct_2);
    CVectorAddElement( vector_2, &struct_2);

    dbus_type_array vector_3;
    CVectorCreate( &vector_3, sizeof( dbus_type_variant ), 2 );
    CVectorAddElement( vector_3, &variant_1 );
    CVectorAddElement( vector_3, &variant_1 );
    
    dbus_type_array vector_4;
    CVectorCreate( &vector_4, sizeof( dbus_type_variant ), 2 );
    dbus_type_variant variant_2 = { .signature = "av",
                                    .ARRAY = vector_3 };
    CVectorAddElement( vector_4, &variant_2 );
    CVectorAddElement( vector_4, &variant_2 );

    struct struct_3 struct_3 = { .struct_1 = { 0xDEADBEEF, string_1 },
                                 .struct_2 = { "STRING_1", "STRING_2" } };

    struct dict_entry_1 dict_entry_1 = { string_1, 0xDEADBEEF };
    struct dict_entry_1 dict_entry_2 = { string_2, 0xBEEFDEAD };
    dbus_type_array vector_5;
    CVectorCreate( &vector_5, sizeof( struct dict_entry_1 ), 2 );
    CVectorAddElement( vector_5, &dict_entry_1 );
    CVectorAddElement( vector_5, &dict_entry_2 );

    dbus_client_call_method( &dbus_client_1, NULL, "method1", "s", string_1 ); 
    dbus_client_call_method_with_reply( &dbus_client_1, NULL, "method2", "s", string_1 ); 
    dbus_client_call_method_with_reply( &dbus_client_1, NULL, "method3", "v", &variant_1 );
    dbus_client_call_method_with_reply( &dbus_client_1, NULL, "method4", "(is)", &struct_1 ); 
    dbus_client_call_method_with_reply( &dbus_client_1, NULL, "method5", "as", vector_1 );
    dbus_client_call_method_with_reply( &dbus_client_1, NULL, "method6", "a(ss)", vector_2); 
    dbus_client_call_method_with_reply( &dbus_client_1, NULL, "method7", "av", vector_3); 
    dbus_client_call_method_with_reply( &dbus_client_1, NULL, "method8", "av", vector_4); 
    dbus_client_call_method_with_reply( &dbus_client_1, NULL, "method9", "((is)(ss))", &struct_3 ); 
    dbus_client_call_method_with_reply( &dbus_client_1, NULL, "method10", "a{si}", vector_5); 

    while( run )
    {
        io_watch();
    }

    CVectorFree( &vector_1 );
    CVectorFree( &vector_2 );
    CVectorFree( &vector_3 );
    CVectorFree( &vector_4 );
    CVectorFree( &vector_5 );

main_dbus_client_register_reply_10_cleanup:
    dbus_client_unregister_reply( &dbus_client_1, "method10" );
main_dbus_client_register_reply_9_cleanup:
    dbus_client_unregister_reply( &dbus_client_1, "method9" );
main_dbus_client_register_reply_8_cleanup:
    dbus_client_unregister_reply( &dbus_client_1, "method8" );
main_dbus_client_register_reply_7_cleanup:
    dbus_client_unregister_reply( &dbus_client_1, "method7" );
main_dbus_client_register_reply_6_cleanup:
    dbus_client_unregister_reply( &dbus_client_1, "method6" );
main_dbus_client_register_reply_5_cleanup:
    dbus_client_unregister_reply( &dbus_client_1, "method5" );
main_dbus_client_register_reply_4_cleanup:
    dbus_client_unregister_reply( &dbus_client_1, "method4" );
main_dbus_client_register_reply_3_cleanup:
    dbus_client_unregister_reply( &dbus_client_1, "method3" );
main_dbus_client_register_reply_2_cleanup:
    dbus_client_unregister_reply( &dbus_client_1, "method2" );
main_dbus_client_register_client_cleanup: 
    dbus_client_unregister_client( &dbus_client_1 );
main_dbus_client_init_cleanup:
    dbus_client_deinit();
main_io_init_cleanup:
    io_deinit();
main_log_init_cleanup:
    log_close();

    return ret;
}

static int c1_reply_handler_2( void *data )
{
    log_debug( "%s(): enter", __func__ );

    dbus_type_string string_1 = NULL;

    dbus_client_get_reply_args( &dbus_client_1, NULL, "method2", "s", &string_1 );
    printf( "%s(no block): %s\n", __func__, string_1 );

//    dbus_client_call_method_with_reply_and_block( &dbus_client_1, "method2", "s", string_1 );
//    dbus_client_get_reply_args( &dbus_client_1, "method2", "s", &string_1 );
    
//    printf( "%s(block): %s\n", __func__, string_1 );

    return 0;
}

static int c1_reply_handler_3( void *data )
{
    log_debug( "%s(): enter", __func__ );

    dbus_type_variant variant_1;

    dbus_client_get_reply_args( &dbus_client_1, NULL, "method3", "v", &variant_1 );
    
    printf( "%s(): %s\n", __func__, variant_1.STRING );

    return 0;
}

static int c1_reply_handler_4( void *data )
{
    log_debug( "%s(): enter", __func__ );
    
    struct struct_1 struct_1;
    struct_1.string_1 = NULL;

    dbus_client_get_reply_args( &dbus_client_1, NULL, "method4", "(is)", &struct_1 );
    
    printf( "%s(): 0x%X, %s\n", __func__, struct_1.uint32_1, struct_1.string_1 );

    return 0;
}

static int c1_reply_handler_5( void *data )
{
    log_debug( "%s(): enter", __func__ );
   
    char *string_1;
    char *string_2;

    dbus_type_array vector_1;

    dbus_client_get_reply_args( &dbus_client_1, NULL, "method5", "as", &vector_1 );

    CVectorGetElement( vector_1, &string_1, 0 );
    CVectorGetElement( vector_1, &string_2, 1 );

    printf( "%s(): %s, %s\n", __func__, string_1, string_2 );

    CVectorFree( &vector_1 );

    return 0;
}

static int c1_reply_handler_6( void *data )
{
    log_debug( "%s(): enter", __func__ );

    dbus_type_array vector_1;

    struct struct_2 struct_1;
    struct struct_2 struct_2;

    dbus_client_get_reply_args( &dbus_client_1, NULL, "method6", "a(ss)", &vector_1 );

    CVectorGetElement( vector_1, &struct_1, 0 );
    CVectorGetElement( vector_1, &struct_2, 1 );

    printf( "%s(): (%s, %s), (%s, %s)\n", __func__, struct_1.string_1, struct_1.string_2, struct_2.string_1, struct_2.string_2 );
    
    CVectorFree( &vector_1 );

    return 0;
}

static int c1_reply_handler_7( void *data )
{
    log_debug( "%s(): enter", __func__ );

    dbus_type_array vector_1;
    
    dbus_type_variant variant_1;
    dbus_type_variant variant_2;
    
    dbus_client_get_reply_args( &dbus_client_1, NULL, "method7", "av", &vector_1  );

    CVectorGetElement( vector_1, &variant_1, 0 );
    CVectorGetElement( vector_1, &variant_2, 1 );
    
    printf( "%s(): %s, %s\n", __func__, variant_1.STRING, variant_2.STRING );
    
    CVectorFree( &vector_1 );
    
    return 0;
}

static int c1_reply_handler_8( void *data )
{
    log_debug( "%s(): enter", __func__ );

    dbus_type_array vector_1;
    
    dbus_client_get_reply_args( &dbus_client_1, NULL, "method8", "av", &vector_1 );    

    dbus_type_variant variant_1;
    dbus_type_variant variant_2;

    CVectorGetElement( vector_1, &variant_1, 0 );
    CVectorGetElement( vector_1, &variant_2, 1 );

    dbus_type_variant variant_3;
    dbus_type_variant variant_4;

    CVectorGetElement( variant_1.ARRAY, &variant_3, 0 );
    CVectorGetElement( variant_1.ARRAY, &variant_4, 1 );

    dbus_type_variant variant_5;
    dbus_type_variant variant_6;

    CVectorGetElement( variant_2.ARRAY, &variant_5, 0 );
    CVectorGetElement( variant_2.ARRAY, &variant_6, 1 );

    printf( "%s(): %s, %s, %s, %s\n", __func__, variant_3.STRING, variant_4.STRING, variant_5.STRING, variant_6.STRING );

    CVectorFree( &variant_1.ARRAY );
    CVectorFree( &variant_2.ARRAY );
    CVectorFree( &vector_1 );
    
    return 0;
}

static int c1_reply_handler_9( void *data )
{
    log_debug( "%s(): enter", __func__ );

    struct struct_3 struct_1;
    
    dbus_client_get_reply_args( &dbus_client_1, NULL, "method9", "((is)(ss))", &struct_1 );    

    printf( "%s(): 0x%X, %s, %s, %s\n", __func__, struct_1.struct_1.uint32_1,
                                                  struct_1.struct_1.string_1, 
                                                  struct_1.struct_2.string_1,
                                                  struct_1.struct_2.string_2 );

    
    return 0;
}

static int c1_reply_handler_10( void *data )
{
    log_debug( "%s(): enter", __func__ );

    dbus_type_array vector_1;

    struct dict_entry_1 dict_entry_1;
    struct dict_entry_1 dict_entry_2;

    dbus_client_get_reply_args( &dbus_client_1, NULL, "method10", "a{si}", &vector_1 );

    CVectorGetElement( vector_1, &dict_entry_1, 0 );
    CVectorGetElement( vector_1, &dict_entry_2, 1 );

    printf( "%s(): (%s, 0x%X), (%s, 0x%X)\n", __func__, dict_entry_1.key, dict_entry_1.value, dict_entry_2.key, dict_entry_2.value );
    
    CVectorFree( &vector_1 );
    
    run = 0;

    return 0;
}


static void sig_int_handler(int signo)
{
    run = 0;
}
