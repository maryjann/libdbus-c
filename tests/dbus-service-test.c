/*
 * ----------------------------------------------------------------------------
 * "THE BEER-WARE LICENSE" (Revision 42):
 * <nowacki.jakub@gmail.com> wrote this file. As long as you retain this notice you
 * can do whatever you want with this stuff. If we meet some day, and you think
 * this stuff is worth it, you can buy me a beer in return Jakub Nowacki
 * ----------------------------------------------------------------------------
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <signal.h>

#include <unistd.h>

#include <ctools/log.h>
#include <ctools/io.h>
#include "dbus-service.h"
#include "dbus-types.h"
#include "dbus-service-1-introspect.h"
#include "dbus-service-2-introspect.h"

DBUS_STRUCT struct_1
{
    dbus_type_uint32 uint32_1;
    dbus_type_string string_1;
};

DBUS_STRUCT struct_2
{
    dbus_type_string string_1;
    dbus_type_string string_2;
};

DBUS_STRUCT struct_3
{
    struct struct_1 struct_1;
    struct struct_2 struct_2;
};

DBUS_STRUCT dict_entry_1
{
    dbus_type_string key;
    dbus_type_uint32 value;
};

static int s1_message_handler_1( uint8_t reply_expected, void *data );
static int s1_message_handler_2( uint8_t reply_expected, void *data );
static int s1_message_handler_3( uint8_t reply_expected, void *data );
static int s1_message_handler_4( uint8_t reply_expected, void *data );
static int s1_message_handler_5( uint8_t reply_expected, void *data );
static int s1_message_handler_6( uint8_t reply_expected, void *data );
static int s1_message_handler_7( uint8_t reply_expected, void *data );
static int s1_message_handler_8( uint8_t reply_expected, void *data );
static int s1_message_handler_9( uint8_t reply_expected, void *data );
static int s1_message_handler_9( uint8_t reply_expected, void *data );
static int s1_message_handler_10( uint8_t reply_expected, void *data );
static int s1_introspect_handler( uint8_t reply_expected, void *data );

static int s2_message_handler_1( uint8_t reply_expected, void *data );
static int s2_introspect_handler( uint8_t reply_expected, void *data );
    
static dbus_service_t dbus_service_1;
static dbus_service_t dbus_service_2;

static void sig_int_handler(int signo);

static int run = 1;

int main( int argc, char *argv[] )
{
    int ret = 0;

    signal(SIGINT, sig_int_handler);
    
    log_init( "dbus-service-test", 1, 7 );
    io_init();

    dbus_service_init( DBUS_BUS_TYPE_SESSION );

    dbus_service_register_service( &dbus_service_1, "/org/freedesktop/DBusService1", "org.freedesktop.DBusService1" );

    dbus_service_register_interface( &dbus_service_1, "org.freedesktop.DBus.Introspectable");
    dbus_service_register_method_on_interface( &dbus_service_1, "org.freedesktop.DBus.Introspectable", "Introspect", s1_introspect_handler );

    dbus_service_register_method( &dbus_service_1, "method1", s1_message_handler_1 );
    dbus_service_register_method( &dbus_service_1, "method2", s1_message_handler_2 );
    dbus_service_register_method( &dbus_service_1, "method3", s1_message_handler_3 );
    dbus_service_register_method( &dbus_service_1, "method4", s1_message_handler_4 );
    dbus_service_register_method( &dbus_service_1, "method5", s1_message_handler_5 );
    dbus_service_register_method( &dbus_service_1, "method6", s1_message_handler_6 );
    dbus_service_register_method( &dbus_service_1, "method7", s1_message_handler_7 );
    dbus_service_register_method( &dbus_service_1, "method8", s1_message_handler_8 );
    dbus_service_register_method( &dbus_service_1, "method9", s1_message_handler_9 );
    dbus_service_register_method( &dbus_service_1, "method10", s1_message_handler_10 );
    
    dbus_service_register_service( &dbus_service_2, "/org/freedesktop/DBusService2", "org.freedesktop.DBusService2" );

    dbus_service_register_interface( &dbus_service_2, "org.freedesktop.DBus.Introspectable");
    dbus_service_register_method_on_interface( &dbus_service_2, "org.freedesktop.DBus.Introspectable", "Introspect", s2_introspect_handler );

    dbus_service_register_method( &dbus_service_2, "method1", s2_message_handler_1 );

    while( run )
    {
        io_watch();
    }

    dbus_service_unregister_method( &dbus_service_1, "method1" );
    dbus_service_unregister_method( &dbus_service_1, "method2" );
    dbus_service_unregister_method( &dbus_service_1, "method3" );
    dbus_service_unregister_method( &dbus_service_1, "method4" );
    dbus_service_unregister_method( &dbus_service_1, "method5" );
    dbus_service_unregister_method( &dbus_service_1, "method6" );
    dbus_service_unregister_method( &dbus_service_1, "method7" );
    dbus_service_unregister_method( &dbus_service_1, "method8" );
    dbus_service_unregister_method( &dbus_service_1, "method9" );
    dbus_service_unregister_method( &dbus_service_1, "method10" );

    dbus_service_unregister_method_on_interface( &dbus_service_1, "org.freedesktop.DBus.Introspectable", "Introspect" );
    dbus_service_unregister_interface( &dbus_service_1, "org.freedesktop.DBus.Introspectable" );
    dbus_service_unregister_service( &dbus_service_1 );
    
    dbus_service_unregister_method( &dbus_service_2, "method1" );
    dbus_service_unregister_method_on_interface( &dbus_service_2, "org.freedesktop.DBus.Introspectable", "Introspect" );
    dbus_service_unregister_interface( &dbus_service_2, "org.freedesktop.DBus.Introspectable" );
    dbus_service_unregister_service( &dbus_service_2 );

    dbus_service_deinit();
    io_deinit();
    log_close();

    return 0;
}

static int s1_message_handler_1( uint8_t reply_expected, void *data )
{
    char *string_1 = 0;
    dbus_service_get_method_args( &dbus_service_1, "method1", "s", &string_1 );
    printf( "%s: %s\n", __func__, string_1 );

    if( reply_expected )
    {
        printf( "Will reply\n" );
        dbus_service_send_reply( &dbus_service_1, "method1", "s", string_1 );
    }
    else
    {
        printf( "Will not reply\n" );
    }
    
    dbus_service_send_signal( &dbus_service_1, "signal1", "s", string_1 );
    
    return 0;
}

static int s1_message_handler_2( uint8_t reply_expected, void *data )
{
    char *string_1 = 0;

    dbus_service_get_method_args( &dbus_service_1, "method2", "s", &string_1 );
    
    printf( "%s: %s\n", __func__, string_1 );
    
    if( reply_expected )
    {
        printf( "Will reply\n" );
        dbus_service_send_reply( &dbus_service_1, "method2", "s", string_1 );
    }
    else
    {
        printf( "Will not reply\n" );
    }
    
    return 0;
}

static int s1_message_handler_3( uint8_t reply_expected, void *data )
{
    dbus_type_variant variant_1;
 
    dbus_service_get_method_args( &dbus_service_1, "method3", "v", &variant_1 );
 
    printf( "%s: %s\n", __func__, variant_1.STRING );
 
    if( reply_expected )
    {
        printf( "Will reply\n" );
        dbus_service_send_reply( &dbus_service_1, "method3", "v", &variant_1 );
    }
    else
    {
        printf( "Will not reply\n" );
    }
    
    return 0;
}

static int s1_message_handler_4( uint8_t reply_expected, void *data )
{
    struct struct_1 struct_1;

    dbus_service_get_method_args( &dbus_service_1, "method4", "(is)", &struct_1 );

    printf( "%s: 0x%X, %s\n", __func__, struct_1.uint32_1, struct_1.string_1 );
    
    if( reply_expected )
    {
        printf( "Will reply\n" );
        dbus_service_send_reply( &dbus_service_1, "method4", "(is)", &struct_1 );
    }
    else
    {
        printf( "Will not reply\n" );
    }
    
    return 0;
}

static int s1_message_handler_5( uint8_t reply_expected, void *data )
{
    dbus_type_array vector_1;

    char *string_1;
    char *string_2;

    dbus_service_get_method_args( &dbus_service_1, "method5", "as", &vector_1 );

    CVectorGetElement( vector_1, &string_1, 0 );
    CVectorGetElement( vector_1, &string_2, 1 );

    printf( "%s: %s, %s\n", __func__, string_1, string_2 );

    if( reply_expected )
    {
        printf( "Will reply\n" );
        dbus_service_send_reply( &dbus_service_1, "method5", "as", vector_1 );
    }
    else
    {
        printf( "Will not reply\n" );
    }

    CVectorFree( &vector_1 );
    
    return 0;
}

static int s1_message_handler_6( uint8_t reply_expected, void *data )
{
    dbus_type_array vector_1;

    struct struct_2 struct_1;
    struct struct_2 struct_2;

    dbus_service_get_method_args( &dbus_service_1, "method6", "a(ss)", &vector_1 );

    CVectorGetElement( vector_1, &struct_1, 0 );
    CVectorGetElement( vector_1, &struct_2, 1 );

    printf( "%s: (%s, %s), (%s, %s)\n", __func__, struct_1.string_1, struct_1.string_2, struct_2.string_1, struct_2.string_2 );

    if( reply_expected )
    {
        printf( "Will reply\n" );
        dbus_service_send_reply( &dbus_service_1, "method6", "a(ss)", vector_1 );
    }
    else
    {
        printf( "Will not reply\n" );
    }

    CVectorFree( &vector_1 );
    
    return 0;
}

static int s1_message_handler_7( uint8_t reply_expected, void *data )
{
    dbus_type_array vector_1;

    dbus_type_variant variant_1;
    dbus_type_variant variant_2;

    dbus_service_get_method_args( &dbus_service_1, "method7", "av", &vector_1 );

    CVectorGetElement( vector_1, &variant_1, 0 );
    CVectorGetElement( vector_1, &variant_2, 1 );

    printf( "%s: %s, %s\n", __func__, variant_1.STRING, variant_2.STRING );

    if( reply_expected )
    {
        printf( "Will reply\n" );
        dbus_service_send_reply( &dbus_service_1, "method7", "av", vector_1 );
    }
    else
    {
        printf( "Will not reply\n" );
    }

    CVectorFree( &vector_1 );
    
    return 0;

}

static int s1_message_handler_8( uint8_t reply_expected, void *data )
{
    dbus_type_array vector_1;
        
    dbus_type_variant variant_1;
    dbus_type_variant variant_2;
    
    dbus_type_variant variant_3;
    dbus_type_variant variant_4;
    dbus_type_variant variant_5;
    dbus_type_variant variant_6;


    dbus_service_get_method_args( &dbus_service_1, "method8", "av", &vector_1 );

    CVectorGetElement( vector_1, &variant_1, 0 );
    CVectorGetElement( vector_1, &variant_2, 1 );

    CVectorGetElement( variant_1.ARRAY, &variant_3, 0 );
    CVectorGetElement( variant_1.ARRAY, &variant_4, 1 );
    
    CVectorGetElement( variant_2.ARRAY, &variant_5, 0 );
    CVectorGetElement( variant_2.ARRAY, &variant_6, 1 );

    printf( "%s: (%s, %s), (%s, %s)\n", __func__, variant_3.STRING, variant_4.STRING, variant_5.STRING, variant_6.STRING );

    if( reply_expected )
    {
        printf( "Will reply\n" );
        dbus_service_send_reply( &dbus_service_1, "method8", "av", vector_1 );    
    }
    else
    {
        printf( "Will not reply\n" );
    }

    CVectorFree( &variant_1.ARRAY );
    CVectorFree( &variant_2.ARRAY );
    CVectorFree( &vector_1 );
    
    return 0;

}

static int s1_message_handler_9( uint8_t reply_expected, void *data )
{
    struct struct_3 struct_1;
    
    dbus_service_get_method_args( &dbus_service_1, "method9", "((is)(ss))", &struct_1 );
    printf( "%s: (0x%X, %s), (%s, %s)\n", __func__, struct_1.struct_1.uint32_1, struct_1.struct_1.string_1, struct_1.struct_2.string_1, struct_1.struct_2.string_2 );

    if( reply_expected )
    {
        printf( "Will reply\n" );
        dbus_service_send_reply( &dbus_service_1, "method9", "((is)(ss))", &struct_1 );
    }
    else
    {
        printf( "Will not reply\n" );
    }
    
    return 0;
}

static int s1_message_handler_10( uint8_t reply_expected, void *data )
{
    dbus_type_array vector_1;

    struct dict_entry_1 dict_entry_1;
    struct dict_entry_1 dict_entry_2;

    dbus_service_get_method_args( &dbus_service_1, "method10", "a{si}", &vector_1 );

    CVectorGetElement( vector_1, &dict_entry_1, 0 );
    CVectorGetElement( vector_1, &dict_entry_2, 1 );

    printf( "%s: (%s, 0x%X), (%s, 0x%X)\n", __func__, dict_entry_1.key, dict_entry_1.value, dict_entry_2.key, dict_entry_2.value );

    if( reply_expected )
    {
        printf( "Will reply\n" );
        dbus_service_send_reply( &dbus_service_1, "method10", "a{si}", vector_1 );
    }
    else
    {
        printf( "Will not reply\n" );
    }

    CVectorFree( &vector_1 );
    
    return 0;
}

static int s1_introspect_handler( uint8_t reply_expected, void *data )
{
    dbus_service_send_reply_on_interface( &dbus_service_1, "org.freedesktop.DBus.Introspectable", "Introspect", "s", ___tests_DBusService1Introspect_xml );
    return 0;
}

static int s2_message_handler_1( uint8_t reply_expected, void *data )
{
    char *string_1 = 0;

    dbus_service_get_method_args( &dbus_service_2, "method1", "s", &string_1 );

    printf( "%s: %s\n", __func__, string_1 );

    if( reply_expected )
    {
        printf( "Will reply\n" );
        dbus_service_send_reply( &dbus_service_2, "method1", "s", string_1 ); 
    }
    else
    {
        printf( "Will not reply\n" );
    }
    
    dbus_service_send_signal( &dbus_service_1, "signal1", "s", string_1 ); 
    
    return 0;
}

static int s2_introspect_handler( uint8_t reply_expected, void *data )
{
    dbus_service_send_reply_on_interface( &dbus_service_2, "org.freedesktop.DBus.Introspectable", "Introspect", "s", ___tests_DBusService2Introspect_xml );
    return 0;
}

static void sig_int_handler(int signo)
{
    run = 0;
}
