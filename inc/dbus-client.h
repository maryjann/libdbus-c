/*
 * ----------------------------------------------------------------------------
 * "THE BEER-WARE LICENSE" (Revision 42):
 * <nowacki.jakub@gmail.com> wrote this file. As long as you retain this notice you
 * can do whatever you want with this stuff. If we meet some day, and you think
 * this stuff is worth it, you can buy me a beer in return Jakub Nowacki
 * ----------------------------------------------------------------------------
 */

#ifndef _DBUS_CLIENT_H_
#define _DBUS_CLIENT_H_

#include <ctools/map.h>

#include "dbus-types.h"

typedef int ( *dbus_client_signal_handler_f ) ( void * const data );
typedef int ( *dbus_client_reply_handler_f ) ( void * const data );

typedef struct dbus_client_s
{
    char *path;
    char *name;
    
    map_t interfaces;
} dbus_client_t;

int dbus_client_init( dbus_bus_type_t bus_type );
void dbus_client_deinit( void );

int dbus_client_register_client( dbus_client_t *dbus_client, char * const path, char * const name );
int dbus_client_unregister_client( dbus_client_t *dbus_client );

int dbus_client_register_interface( dbus_client_t *dbus_client, char * const name );
int dbus_client_unregister_interface( dbus_client_t *dbus_client, char * const name );

int dbus_client_register_signal( dbus_client_t *dbus_client, char * const name, dbus_client_signal_handler_f handler, void *data );
int dbus_client_unregister_signal( dbus_client_t *dbus_client, char * const name );
int dbus_client_register_signal_on_interface( dbus_client_t *dbus_client, char * const ifce_name, char * const name, dbus_client_signal_handler_f handler, void *data );
int dbus_client_unregister_signal_on_interface( dbus_client_t *dbus_client, char * const ifce_name, char * const name );

int dbus_client_get_signal_args( dbus_client_t *dbus_client, char * const name, const char *format, ... );
int dbus_client_get_signal_args_on_interface( dbus_client_t *dbus_client, char * const ifce_name, char * const name, const char *format, ... );

int dbus_client_register_reply( dbus_client_t *dbus_client, char * const name, dbus_client_reply_handler_f handler );
int dbus_client_unregister_reply( dbus_client_t *dbus_client, char * const name );
int dbus_client_register_reply_on_interface( dbus_client_t *dbus_client, char * const ifce_name, char * const name, dbus_client_reply_handler_f handler );
int dbus_client_unregister_reply_on_interface( dbus_client_t *dbus_client, char * const ifce_name, char * const name );

int dbus_client_get_reply_args( dbus_client_t *dbus_client, uint32_t *id, char * const name, const char *format, ... );
int dbus_client_get_reply_args_on_interface( dbus_client_t *dbus_client, uint32_t *id, char * const ifce_name, char * const name, const char * format, ... );

int dbus_client_call_method( dbus_client_t *dbus_client, uint32_t *id, char * const name, const char * format, ... );
int dbus_client_call_method_on_interface( dbus_client_t *dbus_client, uint32_t *id, char * const ifce_name, char * const name, const char * format, ... );

int dbus_client_call_method_with_reply( dbus_client_t *dbus_client, uint32_t *id, char * const name, const char * format, ... );
int dbus_client_call_method_with_reply_on_interface( dbus_client_t *dbus_client, uint32_t *id, char * const ifce_name, char * const name, const char * format, ... );

int dbus_client_call_method_and_block( dbus_client_t *dbus_client, uint32_t *id, char * const name, const char * format, ... );
int dbus_client_call_method_on_interface_and_block( dbus_client_t *dbus_client, uint32_t *id, char * const ifce_name, char * const name, const char * format, ... );

int dbus_client_call_method_with_reply_and_block( dbus_client_t *dbus_client, uint32_t *id, char * const name, const char * format, ... );
int dbus_client_call_method_with_reply_on_interface_and_block( dbus_client_t *dbus_client, uint32_t *id, char * const ifce_name, char * const name, const char * format, ... );

#endif
