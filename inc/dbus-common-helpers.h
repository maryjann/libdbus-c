/*
 * ----------------------------------------------------------------------------
 * "THE BEER-WARE LICENSE" (Revision 42):
 * <nowacki.jakub@gmail.com> wrote this file. As long as you retain this notice you
 * can do whatever you want with this stuff. If we meet some day, and you think
 * this stuff is worth it, you can buy me a beer in return Jakub Nowacki
 * ----------------------------------------------------------------------------
 */

#ifndef _DBUS_COMMON_HELPERS_H_
#define _DBUS_COMMON_HELPERS_H_

int dbus_common_decode_complete_type( const char *in, char *out );
size_t dbus_common_calculate_complete_type_size( const char *signature );

#endif
