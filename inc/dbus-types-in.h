/*
 * ----------------------------------------------------------------------------
 * "THE BEER-WARE LICENSE" (Revision 42):
 * <nowacki.jakub@gmail.com> wrote this file. As long as you retain this notice you
 * can do whatever you want with this stuff. If we meet some day, and you think
 * this stuff is worth it, you can buy me a beer in return Jakub Nowacki
 * ----------------------------------------------------------------------------
 */

#ifndef _DBUS_TYPES_IN_H_
#define _DBUS_TYPES_IN_H_

#include <dbus/dbus-protocol.h>

typedef uint8_t         dbus_type_byte;
typedef uint32_t        dbus_type_boolean;
typedef int16_t         dbus_type_int16;
typedef uint16_t        dbus_type_uint16;
typedef int32_t         dbus_type_int32;
typedef uint32_t        dbus_type_uint32;
typedef int64_t         dbus_type_int64;
typedef uint64_t        dbus_type_uint64;
typedef double          dbus_type_double;
typedef char *          dbus_type_string;
typedef CVectorHandle   dbus_type_array;
typedef void *          dbus_type_struct;
typedef void *          dbus_type_dict_entry;
typedef uint32_t        dbus_type_unix_fd;

typedef struct __attribute__((__packed__)) dbus_variant
{
    char signature[DBUS_MAXIMUM_SIGNATURE_LENGTH + 1/*\0*/];
    union
    {
        void*                   DATA;
        dbus_type_byte          BYTE;
        dbus_type_boolean       BOOLEAN;
        dbus_type_int16         INT16;
        dbus_type_uint16        UINT16;
        dbus_type_int32         INT32;
        dbus_type_uint32        UINT32;
        dbus_type_int64         INT64;
        dbus_type_uint64        UINT64;
        dbus_type_double        DOUBLE;
        dbus_type_string        STRING;
        dbus_type_array         ARRAY;
        dbus_type_struct        STRUCT;
        dbus_type_dict_entry    DICT_ENTRY;
        struct dbus_variant*    VARIANT;
    };
} dbus_type_variant;

typedef enum dbus_data_type_e
{
    DBUS_DATA_TYPE_INVALID = DBUS_TYPE_INVALID,
    DBUS_DATA_TYPE_BYTE = DBUS_TYPE_BYTE,
    DBUS_DATA_TYPE_BOOLEAN = DBUS_TYPE_BOOLEAN,
    DBUS_DATA_TYPE_INT16 = DBUS_TYPE_INT16,
    DBUS_DATA_TYPE_UINT16 = DBUS_TYPE_UINT16,
    DBUS_DATA_TYPE_INT32 = DBUS_TYPE_INT32,
    DBUS_DATA_TYPE_UINT32 = DBUS_TYPE_UINT32,
    DBUS_DATA_TYPE_INT64 = DBUS_TYPE_INT64,
    DBUS_DATA_TYPE_UINT64 = DBUS_TYPE_UINT64,
    DBUS_DATA_TYPE_DOUBLE = DBUS_TYPE_DOUBLE,
    DBUS_DATA_TYPE_STRING = DBUS_TYPE_STRING,
    DBUS_DATA_TYPE_OBJECT_PATH = DBUS_TYPE_OBJECT_PATH,
    DBUS_DATA_TYPE_SIGNATURE = DBUS_TYPE_SIGNATURE,
    DBUS_DATA_TYPE_ARRAY = DBUS_TYPE_ARRAY,
    DBUS_DATA_TYPE_VARIANT = DBUS_TYPE_VARIANT,
    DBUS_DATA_TYPE_STRUCT = DBUS_TYPE_STRUCT,
    DBUS_DATA_TYPE_DICT_ENTRY = DBUS_TYPE_DICT_ENTRY,
    DBUS_DATA_TYPE_UNIX_FD = DBUS_TYPE_UNIX_FD
} dbus_data_type_t;

typedef enum dbus_bus_type_e
{
    DBUS_BUS_TYPE_SYSTEM,
    DBUS_BUS_TYPE_SESSION,
    DBUS_BUS_TYPE_STARTER
} dbus_bus_type_t;

#endif
