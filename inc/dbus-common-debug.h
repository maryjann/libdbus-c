/*
 * ----------------------------------------------------------------------------
 * "THE BEER-WARE LICENSE" (Revision 42):
 * <nowacki.jakub@gmail.com> wrote this file. As long as you retain this notice you
 * can do whatever you want with this stuff. If we meet some day, and you think
 * this stuff is worth it, you can buy me a beer in return Jakub Nowacki
 * ----------------------------------------------------------------------------
 */

#ifndef _DBUS_COMMON_DEBUG_H_
#define _DBUS_COMMON_DEBUG_H_

typedef enum dbus_common_msg_level_e
{
    MSG_LEVEL_STAY = 0,
    MSG_LEVEL_PRE_UP,
    MSG_LEVEL_PRE_DOWN,
    MSG_LEVEL_PRE_RESET,
    MSG_LEVEL_POST_UP,
    MSG_LEVEL_POST_DOWN,
    MSG_LEVEL_POST_RESET,
} dbus_common_msg_level_t;

char * dbus_common_debug_msg( const char * type );
char * dbus_common_debug_basic( const char * type, void * data );
char * dbus_common_get_indent( dbus_common_msg_level_t level );

#endif
