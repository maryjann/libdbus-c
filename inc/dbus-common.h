/*
 * ----------------------------------------------------------------------------
 * "THE BEER-WARE LICENSE" (Revision 42):
 * <nowacki.jakub@gmail.com> wrote this file. As long as you retain this notice you
 * can do whatever you want with this stuff. If we meet some day, and you think
 * this stuff is worth it, you can buy me a beer in return Jakub Nowacki
 * ----------------------------------------------------------------------------
 */

#ifndef _DBUS_COMMON_H_
#define _DBUS_COMMON_H_

#include <dbus/dbus-shared.h>

#include <dbus-types.h>

inline static DBusBusType dbus_common_map_bus_type( dbus_bus_type_t bus_type )
{
    switch( bus_type )
    {
        case DBUS_BUS_TYPE_SYSTEM:
            return DBUS_BUS_SYSTEM;
        case DBUS_BUS_TYPE_SESSION:
            return DBUS_BUS_SESSION;
        case DBUS_BUS_TYPE_STARTER:
            return DBUS_BUS_STARTER;
    }
} 

int dbus_common_parse_message_args( DBusMessageIter *iter, const char * format, va_list va );
int dbus_common_build_message_args( DBusMessageIter *iter, DBusMessageIter *iter_up, int parent, const char *format, va_list va );

#endif
