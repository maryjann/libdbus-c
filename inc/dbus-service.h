/*
 * ----------------------------------------------------------------------------
 * "THE BEER-WARE LICENSE" (Revision 42):
 * <nowacki.jakub@gmail.com> wrote this file. As long as you retain this notice you
 * can do whatever you want with this stuff. If we meet some day, and you think
 * this stuff is worth it, you can buy me a beer in return Jakub Nowacki
 * ----------------------------------------------------------------------------
 */

#ifndef _DBUS_SERVICE_H_
#define _DBUS_SERVICE_H_

#include <ctools/map.h>

#include "dbus-types.h"

typedef int ( *dbus_service_method_handler_f ) ( uint8_t reply_expected, void* data );

typedef struct dbus_service_s
{
    char *path;
    char *name;

    map_t interfaces;
} dbus_service_t;

int dbus_service_init( dbus_bus_type_t bus_type );
void dbus_service_deinit( void );

int dbus_service_register_service( dbus_service_t *dbus_service, char * const path, char * const name );
int dbus_service_unregister_service( dbus_service_t *dbus_service );

int dbus_service_register_interface( dbus_service_t *dbus_service, char * const name );
int dbus_service_unregister_interface( dbus_service_t *dbus_service, char * const name );

int dbus_service_register_method( dbus_service_t *dbus_service, char * const name, dbus_service_method_handler_f handler );
int dbus_service_unregister_method( dbus_service_t *dbus_service, char * const name );
int dbus_service_register_method_on_interface( dbus_service_t *dbus_service, char * const ifce_name, char * const method_name, dbus_service_method_handler_f handler );
int dbus_service_unregister_method_on_interface( dbus_service_t *dbus_service, char * const ifce_name, char * const method_name );

int dbus_service_get_method_args( dbus_service_t *dbus_service, char * const name, char * const format, ... );
int dbus_service_get_method_args_on_interface( dbus_service_t *dbus_service, char * const ifce_name, char * const name, char * const format, ... );

int dbus_service_send_reply( dbus_service_t *dbus_service, char * const name, char * const format, ... );
int dbus_service_send_reply_on_interface( dbus_service_t *dbus_service, char * const ifce_name, char * const name, char * const format, ... );

int dbus_service_send_signal( dbus_service_t *dbus_service, char * const name, char * const format, ... );
int dbus_service_send_signal_on_interface( dbus_service_t *dbus_service, char * const ifce_name, char * const name, char * const format, ... );

int dbus_service_send_error( dbus_service_t *dbus_service, char * const name, char * const error_name, char * const error_message );
int dbus_service_send_error_on_interface( dbus_service_t *dbus_service, char * const ifce_name, char * const name, char * const error_name, char * const error_message );

#endif
